<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class SpecificPrice extends SpecificPriceCore
{
    public  $id_product;
    public  $id_specific_price_rule = 0;
    public  $id_cart = 0;
    public  $id_product_attribute;
    public  $id_shop;
    public  $id_shop_group;
    public  $id_currency;
    public  $id_country;
    public  $id_group;
    public  $id_customer;
    public  $price;
    public  $from_quantity;
    public  $reduction;
    public  $reduction_type;
    public  $from;
    public  $to;	

    
    /*
     * Función para el calculo de precio por zona
     * */
	public static function getSpecificPriceZone($id_product, $id_shop, $id_zone, $id_currency, $id_country, $id_group, $quantity, $id_product_attribute = null, $id_customer = 0, $id_cart = 0, $real_quantity = 0)
	{
            if (!SpecificPrice::isFeatureActive())
                return array();
            /*
            ** The date is not taken into account for the cache, but this is for the better because it keeps the consistency for the whole script.
            ** The price must not change between the top and the bottom of the page
            */

            $key = ((int)$id_product.'-'.(int)$id_shop.'-'.(int)$id_zone.'-'.(int)$id_currency.'-'.(int)$id_country.'-'.(int)$id_group.'-'.(int)$quantity.'-'.(int)$id_product_attribute.'-'.(int)$id_cart.'-'.(int)$id_customer.'-'.(int)$real_quantity);
            if (!array_key_exists($key, SpecificPrice::$_specificPriceCache))
            {
                $now = date('Y-m-d H:i:s');
                $query = '
                SELECT a.id_specific_price, a.id_specific_price_rule, a.id_cart, a.id_product, a.id_shop, a.id_shop_group, 
                a.id_currency, a.id_country, a.id_group, a.id_customer, 
                a.id_product_attribute, b.price, a.from_quantity, a.reduction, 
                a.reduction_type, a.`from`, a.`to`, '
                .SpecificPrice::_getScoreQuery($id_product, $id_shop, $id_currency, $id_country, $id_group, $id_customer).
                'FROM `'._DB_PREFIX_.'specific_price_zone` b
                left join `'._DB_PREFIX_.'specific_price` a on (a.id_product = b.id_product)
                WHERE coalesce(b.price, 0) > 0 and b.`id_product` IN (0, '.(int)$id_product.')
                and b.id_zone = '.(int)$id_zone;
                /*AND a.`id_product_attribute` IN (0, '.(int)$id_product_attribute.')
                AND a.`id_shop` IN (0, '.(int)$id_shop.')
                AND a.`id_currency` IN (0, '.(int)$id_currency.')
                AND a.`id_country` IN (0, '.(int)$id_country.')
                AND a.`id_group` IN (0, '.(int)$id_group.')
                AND a.`id_customer` IN (0, '.(int)$id_customer.')
                and b.id_zone = '.(int)$id_zone.'
                AND
                (
                    (`from` = \'0000-00-00 00:00:00\' OR \''.$now.'\' >= `from`)
                    AND
                    (`to` = \'0000-00-00 00:00:00\' OR \''.$now.'\' <= `to`)
                )
                AND id_cart IN (0, '.(int)$id_cart.') 
                AND IF(`from_quantity` > 1, `from_quantity`, 0) <= ';

                $query .= (Configuration::get('PS_QTY_DISCOUNT_ON_COMBINATION') || !$id_cart || !$real_quantity) ? (int)$quantity : max(1, (int)$real_quantity);			
                $query .= ' ORDER BY `id_product_attribute` DESC, `from_quantity` DESC, `id_specific_price_rule` ASC, `score` DESC';*/
                SpecificPrice::$_specificPriceCache[$key] = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query);
            }
            return SpecificPrice::$_specificPriceCache[$key];
	}
}

