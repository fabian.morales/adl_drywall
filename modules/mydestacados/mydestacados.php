<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class MyDestacados extends Module
{
	protected static $cache_products;

	public function __construct()
	{
		$this->name = 'mydestacados';
		$this->tab = 'front_office_features';
		$this->version = '1.6.2';
		$this->author = 'Fabian Morales';
		$this->need_instance = 0;

		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = 'My Destacados';
		$this->description = 'Bloque de productos destacados';
	}

	public function install()
	{
		$this->_clearCache('*');

		if (!parent::install()
			|| !$this->registerHook('header')
			|| !$this->registerHook('displayHomeTab')
			|| !$this->registerHook('displayHomeTabContent')
		)
			return false;

		return true;
	}

	public function uninstall()
	{
		$this->_clearCache('*');

		return parent::uninstall();
	}	

	public function hookDisplayHeader($params)
	{
		$this->hookHeader($params);
	}

	public function hookHeader($params)
	{
		$this->context->controller->addCSS(($this->_path).'/css/mydestacados.css');
                $this->context->controller->addJS(__PS_BASE_URI__.'js/lightSlider/lightGallery.min.js');
		$this->context->controller->addCSS(__PS_BASE_URI__.'js/lightSlider/lightGallery.min.css');
		$this->context->controller->addJS(__PS_BASE_URI__.'js/lightSlider/lightslider.min.js');
		$this->context->controller->addCSS(__PS_BASE_URI__.'js/lightSlider/lightslider.min.css');
	}

	public function _cacheProducts()
	{
		if (!isset(HomeFeatured::$cache_products))
		{
			$category = new Category((int)Configuration::get('HOME_FEATURED_CAT'), (int)Context::getContext()->language->id);
			$nb = (int)Configuration::get('HOME_FEATURED_NBR');
			if (Configuration::get('HOME_FEATURED_RANDOMIZE'))
				HomeFeatured::$cache_products = $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 8), null, null, false, true, true, ($nb ? $nb : 8));
			else
				HomeFeatured::$cache_products = $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 8), 'position');
		}

		if (HomeFeatured::$cache_products === false || empty(HomeFeatured::$cache_products))
			return false;
	}

	public function hookDisplayHomeTab($params)
	{        
        $category = new Category(20, (int)Context::getContext()->language->id);
        $nb = (int)Configuration::get('HOME_FEATURED_NBR');
        $productos = $category->getProducts((int)Context::getContext()->language->id, 1, 64, 'position');
        //print_r($productos);
    	$this->smarty->assign(array('productos' => $productos));

		return $this->display(__FILE__, 'mydestacados.tpl');
	}

	public function hookDisplayHomeTabContent($params)
	{
		return $this->hookDisplayHomeTab($params);
	}
}
