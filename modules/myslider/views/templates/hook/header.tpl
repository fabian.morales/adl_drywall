<script type="text/javascript">
(function(window, $){
	$(document).ready(function() {
        $("#my-slider").lightSlider({
            item: 1,
            pager: false,
            enableDrag: true,
            controls: true,
            loop: true
        });
	});
})(window, jQuery);
</script>
