{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $productos|@count > 0 && isset($productos) }
<script type="text/javascript">
(function (window, $) {

    function inicioDoc() {
        var sliderPromo = $("#lista-promociones").lightSlider({ 
            item:5,
            pager: false,
            enableDrag: true,
            controls: false,
            loop: true,
            slideMove:1,
            responsive : [
                {
                    breakpoint:800,
                    settings: {
                        item:4,
                        slideMove:1,
                        slideMargin:6,
                      }
                },
                {
                    breakpoint:650,
                    settings: {
                        item:3,
                        slideMove:1,
                        slideMargin:4,
                      }
                },
                {
                    breakpoint:500,
                    settings: {
                        item:2,
                        slideMove:1
                      }
                },
                {
                    breakpoint:340,
                    settings: {
                        item:1,
                        slideMove:1
                      }
                }
            ]
        });
        $("#modulo-promociones .prev").click(function(e) {
            e.preventDefault();
            sliderPromo.goToPrevSlide();
        });
        
        $("#modulo-promociones .next").click(function(e) {
            e.preventDefault();
            sliderPromo.goToNextSlide();
        });
    }
    $(document).ready(inicioDoc);
})(window, jQuery);
</script>
<!--Modulo promociones-->
<div class="row hidden-sm hidden-xs" id="modulo-promociones">
    <div class="col-sm-12">
        <a class="control prev" href="#"></a>
        <a class="control next" href="#"></a>
        <div class="titulo-promociones">Promociones</div>
        <ul class="lista-productos" id="lista-promociones">            
            {foreach from=$productos item=p}
                <li>
                    <div class="item-producto">            
                        <a href="{$p.link}"><img src="{$link->getImageLink($p.link_rewrite, $p.id_image, 'large_default')|escape:'html'}" alt="{$p.legend|escape:html:'UTF-8'}" title="{$p.name|escape:html:'UTF-8'}" /></a>
                        <div class="info-producto">
                            <div class="titulo-producto"><a href="{$p.link}">{$p.name}</a></div>
                            <div class="descripcion-producto"><a href="{$p.link}">{$p.description_short|strip_tags|truncate:65:'...'}</a></div>
                            <div class="precio-producto"><a href="{$p.link}">{displayWtPrice p=$p.price}</a></div>
                            <a class="add-carro" href="{$link->getPageLink('cart')}?qty=1&amp;id_product={$p.id_product}&amp;token={$static_token}&amp;add">
                                A&ntilde;adir al carrito
                            </a>
                        </div>
                    </div>
                </li>
            {/foreach}
        </ul>        
    </div>
</div>
{/if}
