<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class MyPromociones extends Module
{
	function __construct()
	{
		$this->name = 'mypromociones';
		$this->tab = 'front_office_features';
		$this->version = '1.1';
		$this->author = 'Fabian Morales';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = 'My Promociones';
		$this->description = 'Slider de promociones';
	}

	function install()
	{
		if (parent::install() == false ||
			!$this->registerHook('displayHomeTabContent') ||
			!$this->registerHook('displayHomeTab') ||
            !$this->registerHook('displayHeader'))
			return false;
		return true;
	}

    function hookDisplayHeader(){
        $this->context->controller->addCSS(($this->_path).'mypromociones.css', 'all');
    }
    
    public function hookDisplayHomeTabContent($params)
    {
        return $this->hookDisplayHomeTab($params);
    }
        
    function hookDisplayHomeTab($params){
        $productos =  Product::getPricesDrop((int)$params['cookie']->id_lang, 0, 5);
        if (!$productos || empty($productos)){
            $productos = array();
        }
        
        $this->smarty->assign("productos", $productos);
        return $this->display(($this->_path), 'mypromociones.tpl');
    }
}
