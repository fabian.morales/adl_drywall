{if $MENU != ''}
	<!-- Menu -->
	<div class="menu_hrz clearfix">
            <div class="despl-resp hidden-lg hidden-md">
                <a href="#menu_horz">Menu</a>
            </div>
		<ul id="menu_horz" class="clearfix menu-resp ">
			{$MENU}
			{if $MENU_SEARCH}
				<li class="menu_buscador">
					<form id="searchbox" action="{$link->getPageLink('search')|escape:'html'}" method="get">
						<p>
							<input type="hidden" name="controller" value="search" />
							<input type="hidden" value="position" name="orderby"/>
							<input type="hidden" value="desc" name="orderway"/>
							<input type="text" name="search_query" value="{if isset($smarty.get.search_query)}{$smarty.get.search_query|escape:'html':'UTF-8'}{/if}" placeholder="..." />
                            <!--span id="boton-buscar">&nbsp;</span-->
                            <input type="submit" id="boton-buscar" value=" " />
						</p>
					</form>
				</li>
			{/if}
		</ul>
	</div>
	<!--/ Menu -->
{/if}