<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class MyProveedores extends Module
{
    function __construct()
    {
        $this->name = 'myproveedores';
        $this->tab = 'front_office_features';
        $this->version = '1.1.1';
		$this->author = 'Fabian Morales';
		$this->need_instance = 0;

		parent::__construct();	

		$this->displayName = 'My Proveedores';
        $this->description = 'Bloque de proveedores';
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

	function install()
	{
		if (parent::install() == false ||
			!$this->registerHook('displayHomeTabContent') ||
			!$this->registerHook('displayHomeTab') ||
            !$this->registerHook('displayHeader'))
			return false;
		return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall())
			return false;

		return true;
	}
	
	function hookDisplayHomeTab($params)
	{
		$id_lang = (int)Context::getContext()->language->id;
		$this->smarty->assign(array(
			'proveedores' => Supplier::getSuppliers(false, $id_lang),
			'link' => $this->context->link,
            'url_prov' =>  __PS_BASE_URI__.'img/su/'
		));
			
		return $this->display(__FILE__, 'myproveedores.tpl');
	}	

	public function hookDisplayHomeTabContent($params)
	{
		return $this->hookDisplayHomeTab($params);
	}

	public function hookDisplayHeader($params)
	{
		$this->context->controller->addCSS(($this->_path).'myproveedores.css', 'all');
	}
}
