{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $proveedores|@count > 0 }
<script type="text/javascript">
(function (window, $) {

    function inicioDoc() {
        $("#lista-proveedores").lightSlider({ 
            item:4,
            auto: true,
            pager: false,
            enableDrag: true,
            controls: true,
            loop: true,
            slideMove:1,
            responsive : [
                {
                    breakpoint:800,
                    settings: {
                        item:3,
                        slideMove:1,
                        slideMargin:6,
                      }
                },
                {
                    breakpoint:500,
                    settings: {
                        item:2,
                        slideMove:1
                      }
                },
                {
                    breakpoint:340,
                    settings: {
                        item:1,
                        slideMove:1
                      }
                }
            ]
        });        
    }
    $(document).ready(inicioDoc);
})(window, jQuery);
</script>
<!--Modulo proveedores-->
<div class="row hidden-sm hidden-xs">
    <div class="col-sm-12">
        <div id="lista-proveedores-cont">
            <ul class="lista-proveedores" id="lista-proveedores">            
                {foreach from=$proveedores item=p}
                    <li>
                        <a href="{$link->getsupplierLink($p.id_supplier, $p.link_rewrite)|escape:'html'}" title="{$p.name}">
                            <img src="{$url_prov|escape:'html'}{$p.id_supplier}-home_default.jpg" alt="{$p.name|escape:html:'UTF-8'}" title="{$p.name|escape:html:'UTF-8'}" />
                        </a>                        
                    </li>
                {/foreach}
            </ul>
        </div>        
    </div>
</div>
{/if}
