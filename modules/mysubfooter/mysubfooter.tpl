{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Modulo sub-footer -->
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="titulo">Informaci&oacute;n</div>
            <ul>
                <li><a href="#">Los m&aacute;s vendidos</a></li>
                <!--li><a href="#">Promociones especiales</a></li-->
                <li><a href="#">Novedades</a></li>
            </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="titulo">Acerca de ADL Drywall</div>
            <ul>
                <li><a href="{$basedir}content/21-quienes-somos">Quiénes somos</a></li>
                <!--li><a href="#">ADL Drywall</a></li-->
                <li><a href="http://www.adieladelombana.com.co" target="_blank">ADL Decoraci&oacute;n</a></li>
                <li><a href="{$basedir}index.php?controller=custom&vista=obras_home">Obras</a></li>
            </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="titulo">Contáctenos</div>
            <ul>
                <li><a href="mailto:contacto@adl.co" target="_blank">contacto@adl.co</a></li>
                <li><a href="#">57(2) 898 0033</a></li>
                <!--li><a href="#">Ayuda en vivo</a></li-->
                <li><a href="{$basedir}index.php?controller=custom&vista=sede_ppal">Nuestras sedes</a></li>
            </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="titulo">S&iacute;guenos</div>
            <ul class="redes-sociales">
                <li class="facebook"><a href="https://www.facebook.com/adldrywallcol?fref=ts" target="_blank">Facebook</a></li>
                <!--li class="twitter"><a href="#">Twitter</a></li-->
                <li class="g-plus"><a href="https://plus.google.com/109343091198450681135/" target="_blank">Google plus</a></li>
            </ul>
        </div>
    </div>
    <div class="row separador">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div id="sello-experiencia"></div>
        </div>
        <div class="col-lg-5 col-md-5 hidden-sm hidden-xs">&nbsp;</div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <!--div id="logos-tarjetas"></div-->
            <div class="info-contacto">
                (c) 2015 | Todos los derechos reservados<br />
                
                Oficina - Bodega principal Carrera 4 No. 22-74<br />
                Cali - Colombia
            </div>
        </div>
    </div>
</div>