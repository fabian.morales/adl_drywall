<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class PrecioZona extends Module
{
    function __construct()
    {
        $this->name = 'preciozona';		
        $this->version = '1.1';
        $this->author = 'FrontierSoft';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->l('Precio Zona');
        $this->description = $this->l('Asigna precios a productos por zona');
    }

    function install()
    {
        if (parent::install() == false
                || $this->registerHook('displayAdminProductsExtra') == false)
            return false;
        return true;
    }	

    /**
    * Returns module content for left column
    *
    * @param array $params Parameters
    * @return string Content
    *
    */

    function hookDisplayAdminProductsExtra($params)
    {
        $idProducto = (int)Tools::getValue('id_product', null);
        $token = Tools::getAdminTokenLite('AdminPrecioZona');        
        $this->smarty->assign(array('token' => $token, 
                                    'tab' => 'AdminPrecioZona',
                                    'idProducto' => $idProducto,
                                    'zonas' => $this->dataListaZona($idProducto)));
        return $this->display(__FILE__, 'preciozona.tpl');
    }
    
    public function dataListaZona($idProducto){
        $db = Db::getInstance();
		$sql = "select a.name, a.id_zone, b.id_product, b.price from "._DB_PREFIX_."zone a ".
                "left join "._DB_PREFIX_."specific_price_zone b on (a.id_zone = b.id_zone and b.id_product = ".$idProducto.") where a.active = 1";
        return $db->executeS($sql);
    }
}
