{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Block tags module -->
<h1>Asignaci&oacute;n de precios por zona</h1>
<br/>
<table>
    <thead>
        <tr>
            <th>Zona</th>
            <th>Precio<br /></th>
        </tr>
    </thead>
    <tbody>
    {foreach from=$zonas item=zona}
        <tr>
            <td>{$zona.name}&nbsp;</td>
            <td><input type="text" name="zona[{$zona.id_zone}]" value="{$zona.price}" /><br /></td>
        </tr>
    {/foreach}
    </tbody>
</table>
<button type="submit" name="submitAddproduct" class="btn btn-default pull-right"><i class="process-icon-save"></i> Guardar</button>
<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i> Guardar y permanecer</button>