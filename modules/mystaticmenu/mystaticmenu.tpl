<div class="menu_hrz clearfix">
    <div class="despl-resp hidden-lg hidden-md">
        <a href="#menu_horz">Menu</a>
    </div>  

    <ul id="menu_horz" class="clearfix menu-resp">
        <li><a href="{$base_dir}index.php" title="Inicio">Inicio</a></li>

        <li>
            <span class="link">Nuestra Empresa</span>
          <ul>
            <li>
              <a href=
              "{$base_dir}content/category/12-nosotros">Nosotros</a>

              <ul>
                <li><a href=
                "{$base_dir}content/21-quienes-somos">Qui&eacute;nes
                Somos</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=hojavida"
                title="Bolsa de empleo">Bolsa de empleo</a></li>

                <li><a href=
                "{$base_dir}content/10-politica-de-tratamiento-de-datos">Pol&iacute;tica
                de tratamiento de datos</a></li>

                <li><a href=
                "{$base_dir}content/11-aviso-de-privacidad">Aviso de
                privacidad</a></li>
              </ul>
            </li>

            <li>
              <a href=
              "{$base_dir}content/category/13-puntos-de-venta">Puntos de
              venta</a>

              <ul>
                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=sede_ppal"
                title="Cali">Cali</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=bogota1"
                title="Bogot&aacute;">Bogot&aacute;</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=pereira1"
                title="Pereira">Pereira</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=armenia"
                title="Armenia">Armenia</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=pasto"
                title="Pasto">Pasto</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=manizales"
                title="Manizales">Manizales</a></li>
              </ul>
            </li>
          </ul>
        </li>

        <li>
            <span class="link">Productos</span>
          <ul>
            <li>
              <a href="{$base_dir}42-placas" title="Placas">Placas</a>

              <ul>
                <li><a href="{$base_dir}12-placas-de-yeso" title=
                "Placas de yeso">Placas de yeso</a></li>

                <li><a href="{$base_dir}13-placas-de-fibrocemento" title=
                "Placas de fibrocemento">Placas de fibrocemento</a></li>

                <li><a href="{$base_dir}52-otras-placas" title=
                "Otras placas">Otras placas</a></li>
              </ul>
            </li>

            <li>
              <a href="{$base_dir}14-perfiles-drywall" title=
              "Perfiles Drywall">Perfiles Drywall</a>

              <ul>
                <li><a href="{$base_dir}38-perfileria-cielos" title=
                "Perfiler&iacute;a cielos">Perfiler&iacute;a cielos</a></li>

                <li><a href="{$base_dir}39-perfileria-muros" title=
                "Perfiler&iacute;a muros">Perfiler&iacute;a muros</a></li>
              </ul>
            </li>

            <li>
              <a href="{$base_dir}16-tornillos-y-elementos-de-fijacion"
              title="Tornillos y elementos de fijaci&oacute;n">Tornillos y elementos de
              fijaci&oacute;n</a>

              <ul>
                <li><a href="{$base_dir}29-tornillos-para-panel-yeso"
                title="Tornillos para panel yeso">Tornillos para panel yeso</a></li>

                <li><a href="{$base_dir}30-tornillos-para-estructura"
                title="Tornillos para estructura">Tornillos para estructura</a></li>

                <li><a href="{$base_dir}31-tornillos-para-fibrocemento"
                title="Tornillos para fibrocemento">Tornillos para fibrocemento</a></li>
              </ul>
            </li>

            <li>
              <a href="{$base_dir}24-cintas" title="Cintas">Cintas</a>

              <ul>
                <li><a href="{$base_dir}25-cinta-esquinera" title=
                "Cinta esquinera">Cinta esquinera</a></li>

                <li><a href="{$base_dir}26-cinta-papel" title=
                "Cinta papel">Cinta papel</a></li>

                <li><a href="{$base_dir}27-cinta-malla" title=
                "Cinta malla">Cinta malla</a></li>

                <li><a href="{$base_dir}28-cinta-metalica" title=
                "Cinta met&aacute;lica">Cinta met&aacute;lica</a></li>

                <li><a href="{$base_dir}56-cinta-de-enmascarar" title=
                "Cinta de enmascarar">Cinta de enmascarar</a></li>
              </ul>
            </li>

            <li>
              <a href="{$base_dir}15-tratamiento-de-juntas" title=
              "Tratamiento de juntas">Tratamiento de juntas</a>

              <ul>
                <li><a href="{$base_dir}33-masillas-en-polvo" title=
                "Masillas en polvo">Masillas en polvo</a></li>

                <li><a href="{$base_dir}34-masillas-premezcladas" title=
                "Masillas premezcladas">Masillas premezcladas</a></li>

                <li><a href="{$base_dir}21-pinturas" title=
                "Pinturas">Pinturas</a></li>

                <li><a href="{$base_dir}22-sellantes" title=
                "Sellantes">Sellantes</a></li>

                <li><a href="{$base_dir}66-revestimientos" title=
                "Revestimientos">Revestimientos</a></li>
              </ul>
            </li>

            <li>
              <a href="{$base_dir}17-productos-termo-acusticos" title=
              "Productos Termo Ac&uacute;sticos">Productos Termo Ac&uacute;sticos</a>

              <ul>
                <li><a href="{$base_dir}61-rollos" title=
                "Rollos">Rollos</a></li>

                <li><a href="{$base_dir}62-laminas" title=
                "L&aacute;minas">L&aacute;minas</a></li>
              </ul>
            </li>

            <li>
              <a href="{$base_dir}19-cielo-falso-desmontable" title=
              "Cielo falso desmontable">Cielo falso desmontable</a>

              <ul>
                <li><a href="{$base_dir}36-fibra-mineral" title=
                "Fibra mineral">Fibra mineral</a></li>

                <li><a href="{$base_dir}37-perfileria-de-autoensamble"
                title="Perfileria de autoensamble">Perfileria de autoensamble</a></li>

                <li><a href="{$base_dir}57-fibrocemento" title=
                "Fibrocemento">Fibrocemento</a></li>

                <li><a href="{$base_dir}58-yeso" title="Yeso">Yeso</a></li>

                <li><a href="{$base_dir}59-fibra-de-vidrio" title=
                "Fibra de vidrio">Fibra de vidrio</a></li>

                <li><a href="{$base_dir}60-metalicos" title=
                "Met&aacute;licos">Met&aacute;licos</a></li>
              </ul>
            </li>

            <li>
              <a href="{$base_dir}18-accesorios-drywall" title=
              "Accesorios Drywall">Accesorios Drywall</a>

              <ul>
                <li><a href="{$base_dir}35-perfiles-plasticos" title=
                "Perfiles pl&aacute;sticos">Perfiles pl&aacute;sticos</a></li>

                <li><a href="{$base_dir}32-parches" title=
                "Parches">Parches</a></li>

                <li><a href="{$base_dir}53-tapas-de-inspeccion" title=
                "Tapas de inspecci&oacute;n">Tapas de inspecci&oacute;n</a></li>

                <li><a href="{$base_dir}54-rejillas" title=
                "Rejillas">Rejillas</a></li>
              </ul>
            </li>

            <li>
              <a href="{$base_dir}23-herramientas" title=
              "Herramientas">Herramientas</a>

              <ul>
                <li><a href="{$base_dir}47-herramientas-manuales" title=
                "Manuales">Manuales</a></li>

                <li><a href="{$base_dir}48-herramientas-electricas" title=
                "El&eacute;ctricas">El&eacute;ctricas</a></li>

                <li><a href="{$base_dir}49-accesorios" title=
                "Accesorios">Accesorios</a></li>

                <li><a href="{$base_dir}55-abrasivos" title=
                "Abrasivos">Abrasivos</a></li>
              </ul>
            </li>

            <li>
              <a href="{$base_dir}50-cubiertas-sin-asbesto" title=
              "Cubiertas sin asbesto">Cubiertas sin asbesto</a>

              <ul>
                <li><a href="{$base_dir}44-tejas" title=
                "Tejas">Tejas</a></li>

                <li><a href="{$base_dir}51-accesorios" title=
                "Accesorios">Accesorios</a></li>
              </ul>
            </li>

            <li>
              <a href="{$base_dir}63-impermeabilizantes" title=
              "Impermeabilizantes">Impermeabilizantes</a>

              <ul>
                <li><a href="{$base_dir}43-mantos" title=
                "Mantos">Mantos</a></li>

                <li><a href="{$base_dir}65-cintas" title=
                "Cintas">Cintas</a></li>
              </ul>
            </li>
          </ul>
        </li>

        <li><a href="{$base_dir}index.php?controller=custom&amp;vista=obras_home" title=
        "Obras">Obras</a></li>

        <li>
            <span class="link">Servicios</span>

          <ul>
            <li>
              <a href=
              "{$base_dir}content/category/2-informacion-interes">Informaci&oacute;n
              de inter&eacute;s</a>

              <ul>
                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=rut"
                title="R.U.T.">R.U.T.</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=creditos"
                title="Solicitudes de cr&eacute;dito">Solicitudes de cr&eacute;dito</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=capacitaciones_home"
                title="Capacitaciones">Capacitaciones</a></li>
              </ul>
            </li>

            <li>
              <a href=
              "{$base_dir}content/category/4-servicio-cliente">Servicio al
              cliente</a>

              <ul>
                <li><a href="{$base_dir}mi-cuenta" title="Mi cuenta">Mi
                cuenta</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=faqs"
                title="Preguntas Frecuentes">Preguntas Frecuentes</a></li>

                <li><a href="#" title="Ayuda en vivo" rel="ayuda">Ayuda en vivo</a></li>
              </ul>
            </li>
          </ul>
        </li>

        <li><a href=
        "{$base_dir}index.php?controller=custom&amp;vista=videos&amp;categoria=8"
        title="Videos">Videos</a></li>

        <li><a href="{$base_dir}blog.html" title=
        "Aprende con ADL">Aprende con ADL</a></li>
        
        <li>
            <span class="link">Cont&aacute;ctenos</span>
          <ul>
            <li>
              <a href=
              "{$base_dir}content/category/5-donde-encontrarnos">&iquest;D&oacute;nde
              encontrarnos?</a>

              <ul>
                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=contacto&amp;ciudad=cali"
                title="Cali">Cali</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=contacto&amp;ciudad=bogota"
                title="Bogot&aacute;">Bogot&aacute;</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=contacto&amp;ciudad=pereira"
                title="Pereira">Pereira</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=contacto&amp;ciudad=armenia"
                title="Armenia">Armenia</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=contacto&amp;ciudad=pasto"
                title="Pasto">Pasto</a></li>

                <li><a href=
                "{$base_dir}index.php?controller=custom&amp;vista=contacto&amp;ciudad=manizales"
                title="Manizales">Manizales</a></li>
              </ul>
            </li>
          </ul>
        </li>

        <li class="menu_buscador">
          <form id="searchbox" action="{$base_dir}buscar" method="get">
            <p><input type="hidden" name="controller" value="search" /> <input type="hidden"
            value="position" name="orderby" /> <input type="hidden" value="desc" name=
            "orderway" /> <input type="text" name="search_query" value="" placeholder=
            "..." /> <!--span id="boton-buscar">&nbsp;</span-->
             <input type="submit" id="boton-buscar" value=" " /></p>
          </form>
        </li>
      </ul>
</div>