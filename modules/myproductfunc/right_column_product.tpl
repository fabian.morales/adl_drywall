{if $mostrar == true}
<script>
    (function($, window){        
        $(document).ready(function() {
            $.fancybox({
                'href': '#inline_camp',
                'autoSize': true,
                'autoHeight': true,
                'autoWidth': true,
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                'speedIn': 500,
                'speedOut': 300,
                'autoDimensions': true,
                'centerOnScroll': true // remove the trailing comma!!
            });
        });
    })(jQuery, window);
</script>

<div id="inline_camp" class="container" style="min-width: 320px; min-height: 300px; overflow-x: hidden;">
    <form action="{$base_dir}/index.php?controller=custom&tarea=camp" method="post">
        <input type="hidden" id="id_producto_camp" name="id_producto_camp" value="{$producto}" />
        <input type="hidden" id="vista" name="vista" value="respuesta_form" />
        <div class="row">
            <div class="col-sm-12">
                <h1>{$categoria->name[1]}</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                {$categoria->description[1]}
                <hr />
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <label for="nombre_camp">Nombre</label>
            </div>
            <div class="col-md-7">
                <input type="text" id="nombre_camp" name="nombre_camp" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-5">
                <label for="email_camp">Correo electr&oacute;nico</label>
            </div>
            <div class="col-md-7">
                <input type="text" id="email_camp" name="email_camp" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-5">
                <label for="observaciones_camp">Nombre</label>
            </div>
            <div class="col-md-7">
                <textarea id="observaciones_camp" name="observaciones_camp"></textarea>
            </div>            
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
                <input type="submit" value="Enviar" class="boton rojo right" />
            </div>
        </div>
    </form>
    
</div>
{/if}