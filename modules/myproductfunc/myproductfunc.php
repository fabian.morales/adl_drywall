<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_CAN_LOAD_FILES_'))
	exit;

class MyProductFunc extends Module
{
    public function __construct()
    {
        $this->name = 'myproductfunc';
        if (version_compare(_PS_VERSION_, '1.4.0.0') >= 0)
                $this->tab = 'front_office_features';
        else
                $this->tab = 'Blocks';
        $this->version = '2.1.1';
        $this->author = 'Fabian Morales';

        $this->bootstrap = true;
        parent::__construct();	

        $this->displayName = 'My Product Func';
        $this->description = 'Informacion y botones funcionales de productos';
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        return parent::install() &&			
        $this->registerHook('displayProductListFunctionalButtons') &&
        $this->registerHook('displayRightColumnProduct') &&
        $this->registerHook('displayProductHover') &&
        $this->registerHook('header');
    }


    public function uninstall()
    {
        // Delete configuration
        parent::uninstall();
    }

    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS(($this->_path).'myproductfunc.css', 'all');
        $this->context->controller->addJS(($this->_path).'myproductfunc.js');
        $this->context->controller->addJS(($this->_path).'ajax-wishlist.js');
    }

    public function hookDisplayProductListFunctionalButtons($params)
    {
        $this->smarty->assign('producto', $params['product']);
        //$this->context->controller->addCSS($this->_path.'style.css', 'all');
        return $this->display(__FILE__, 'myproductfunc.tpl');
    }

    public function hookDisplayProductHover($params)
    {
        $this->smarty->assign('producto', $params['product']);
                
        //$this->context->controller->addCSS($this->_path.'style.css', 'all');
        return $this->display(__FILE__, 'hover.tpl');
    }
    
    public function hookDisplayRightColumnProduct($params){

        $producto = sizeof($params['product']) ? (int)$params['product'] : (int)Tools::getValue('id_product');
        $this->smarty->assign('producto', $producto);
        
        $cat = New Category(68);
        //print_r($cat) and die();
        $this->smarty->assign('categoria', $cat);
        
        $mostrar = Product::idIsOnCategoryId($producto, array(array('id_category' => $cat->id)));
        $this->smarty->assign('mostrar', $mostrar);

        //$this->context->controller->addCSS($this->_path.'style.css', 'all');
        return $this->display(__FILE__, 'right_column_product.tpl');
    }
}
