<!-- Block user information module NAV  -->

    <ul class="user_links">

        <li><a href="" rel="ayuda">Ayuda en vivo</a></li>

        
        {if $is_logged}

            <li>

                <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow"><span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span></a>

            </li>

        {/if}

        <li>

            {if $is_logged}

                <a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">

                    {l s='Sign out' mod='blockuserinfo'}

                </a>

            {else}

                <a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Login to your customer account' mod='blockuserinfo'}">

                    {l s='Sign in' mod='blockuserinfo'}

                </a>

            {/if}

        </li>

        <li><a href="{$base_dir}index.php?controller=custom&vista=faqs">Preguntas Frecuentes</a></li>

        <li><a href="{$base_dir}index.php?controller=custom&vista=contacto&ciudad=cali">Cont&aacute;ctenos</a></li>

    </ul>

<!-- /Block usmodule NAV -->