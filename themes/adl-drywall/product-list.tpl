{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if isset($products) && $products}
	{*define numbers of product per line in other page for desktop*}
	{if $page_name !='index' && $page_name !='product'}
		{assign var='nbItemsPerLine' value=3}
		{assign var='nbItemsPerLineTablet' value=2}
		{assign var='nbItemsPerLineMobile' value=3}
	{else}
		{assign var='nbItemsPerLine' value=4}
		{assign var='nbItemsPerLineTablet' value=3}
		{assign var='nbItemsPerLineMobile' value=2}
	{/if}
	{*define numbers of product per line in other page for tablet*}
	{assign var='nbLi' value=$products|@count}
	{math equation="nbLi/nbItemsPerLine" nbLi=$nbLi nbItemsPerLine=$nbItemsPerLine assign=nbLines}
	{math equation="nbLi/nbItemsPerLineTablet" nbLi=$nbLi nbItemsPerLineTablet=$nbItemsPerLineTablet assign=nbLinesTablet}
	<!-- Products list -->
    <div class="container lista-productos-cat">
    	<div {if isset($id) && $id} id="{$id}"{/if} class="row{if isset($class) && $class} {$class}{/if}">
    	{foreach from=$products item=p name=products}
    		{math equation="(total%perLine)" total=$smarty.foreach.products.total perLine=$nbItemsPerLine assign=totModulo}
    		{math equation="(total%perLineT)" total=$smarty.foreach.products.total perLineT=$nbItemsPerLineTablet assign=totModuloTablet}
    		{math equation="(total%perLineT)" total=$smarty.foreach.products.total perLineT=$nbItemsPerLineMobile assign=totModuloMobile}
    		{if $totModulo == 0}{assign var='totModulo' value=$nbItemsPerLine}{/if}
    		{if $totModuloTablet == 0}{assign var='totModuloTablet' value=$nbItemsPerLineTablet}{/if}
    		{if $totModuloMobile == 0}{assign var='totModuloMobile' value=$nbItemsPerLineMobile}{/if}
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                <div class="item-producto">
                    {if Product::isDiscounted($p.id_product)}
                        <span class="sale-box no-print">
                            <span class="sale-label">{l s='Sale!'}</span>
                        </span>
                    {/if}
                    <a href="{$p.link}"><img src="{$link->getImageLink($p.link_rewrite, $p.id_image, 'large_default')|escape:'html'}" alt="{$p.legend|escape:html:'UTF-8'}" title="{$p.name|escape:html:'UTF-8'}" /></a>
                    <div class="info-producto dd">
                        <div class="titulo-producto"><a href="{$p.link}">{$p.name}</a></div>
                        <div class="descripcion-producto"><a href="{$p.link}">{$p.description_short|strip_tags|truncate:65:'...'}</a></div>
                        <div class="precio-producto"><a href="{$p.link}">{displayWtPrice p=$p.price}</a></div>
                        <a class="add-carro" href="{$link->getPageLink('cart')}?qty=1&amp;id_product={$p.id_product}&amp;token={$static_token}&amp;add">
                            A&ntilde;adir al carrito
                        </a>
                    </div>
                </div>
            </div>
    	{/foreach}
    	</div>
    </div>
{addJsDefL name=min_item}{l s='Please select at least one product' js=1}{/addJsDefL}
{addJsDefL name=max_item}{l s='You cannot add more than %d product(s) to the product comparison' sprintf=$comparator_max_item js=1}{/addJsDefL}
{addJsDef comparator_max_item=$comparator_max_item}
{addJsDef comparedProductsIds=$compared_products}
{/if}