{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='My account'}{/capture}

<div class="titulo-std">{l s='My account'}</div>
{if isset($account_created)}
	<p class="alert alert-success">
		{l s='Your account has been created.'}
	</p>
{/if}
<p class="info-account">Bienvenido a su cuenta, desde aqu&iacute; puedes administrar tu direcciones y pedidos.</p>
<div class="row">
    {if $has_customer_an_address}
	<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">        
        <a class="boton-cuenta" href="{$link->getPageLink('address', true)|escape:'html':'UTF-8'}" title="{l s='Add my first address'}"><img src="{$base_dir}/themes/adl-drywall/images/mis-direcciones.png" /><span>{l s='Add my first address'}</span></a>
    </div>
    {/if}
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
        <a class="boton-cuenta" href="{$link->getPageLink('history', true)|escape:'html':'UTF-8'}" title="{l s='Orders'}"><img src="{$base_dir}/themes/adl-drywall/images/historial-pedidos.png" /><span>Historial de mis pedidos</span></a>
    </div>
    {if $returnAllowed}
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
        <a class="boton-cuenta" href="{$link->getPageLink('order-follow', true)|escape:'html':'UTF-8'}" title="{l s='Merchandise returns'}"><img src="{$base_dir}/themes/adl-drywall/images/historial-pedidos.png" /><span>{l s='My merchandise returns'}</span></a>
    </div>
    {/if}
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
        <a class="boton-cuenta" href="{$link->getPageLink('order-slip', true)|escape:'html':'UTF-8'}" title="{l s='Credit slips'}"><img src="{$base_dir}/themes/adl-drywall/images/vales-descuento.png" /><span>{l s='My credit slips'}</span></a>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
        <a class="boton-cuenta" href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}" title="{l s='Addresses'}"><img src="{$base_dir}/themes/adl-drywall/images/mis-direcciones.png" /><span>{l s='My addresses'}</span></a>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
        <a class="boton-cuenta" href="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" title="{l s='Information'}"><img src="{$base_dir}/themes/adl-drywall/images/datos-personales.png" /><span>{l s='My personal information'}</span></a>
    </div>
{if $voucherAllowed || isset($HOOK_CUSTOMER_ACCOUNT) && $HOOK_CUSTOMER_ACCOUNT !=''}
	<div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
        {if $voucherAllowed}
            <a class="boton-cuenta" href="{$link->getPageLink('discount', true)|escape:'html':'UTF-8'}" title="{l s='Vouchers'}"><img src="{$base_dir}/themes/adl-drywall/images/vales-descuento.png" /><span>{l s='My vouchers'}</span></a>
        {/if}
        {$HOOK_CUSTOMER_ACCOUNT}
    </div>
{/if}
</div>
<div class="footer_links clearfix">
    <a class="boton gris derecha" href="{$base_dir}" title="{l s='Home'}">{l s='Home'} &gt;&gt;</a>
</div>
