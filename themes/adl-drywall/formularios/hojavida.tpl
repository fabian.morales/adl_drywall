<script>
(function($, window){
    $(document).ready(function() {        
    	$("div.boton_faq").click(function() {
            if ($(this).parent().hasClass('activo')){
                $(".acordeon.activo").removeClass('activo');
            }
            else{
                $(".acordeon.activo").removeClass('activo');
                $(this).parent().addClass('activo');   
            }
        });
    });
})(jQuery, window);    
</script>
{capture name=path}Bolsa de empleo{/capture}
<div class="container">
    <div class="row">
        <div class="col-md-6 col-lg-5">
            <h3 class="titulo">Bolsa de empleo</h3>
            <p>Revise nuestras vacantes activas y sí está interesado en aplicar a alguna, por favor envi&eacute;nos su hoja de vida.</p>
            <br />
            <p>Por favor diligencie el siguiente formulario y nos pondremos en contacto con usted.</p>
            <p>Los campos marcados con asterisco (*) son requeridos. </p>
            <br />
            <form method="post" class="form" action="{$base_dir}/index.php?controller=custom&tarea=hojavida" enctype="multipart/form-data">
                <input type="hidden" name="destino" id="destino" value="{$destino}" />
                <input type="hidden" name="vista" id="vista" value="respuesta_form" />
                <ul class="row">
                    <li class="col-md-4"><label for="nombre" required>* Nombre</label></li>
                    <li class="col-md-8"><input type="text" name="nombre" id="nombre" required /></li>                    

                    <li class="col-md-4"><label for="telefono">Tel&eacute;fono</label></li>
                    <li class="col-md-8"><input type="text" name="telefono" id="telefono" /></li>                    

                    <li class="col-md-4"><label for="email" required>* Email</label></li>
                    <li class="col-md-8"><input type="email" name="email" id="email" required /></li>

                    <li class="col-md-4"><label for="asunto">* Adjunte hoja de vida</label></li>
                    <li class="col-md-8"><input type="file" name="archivo" id="archivo" /></li>

                    <li class="col-md-12">
                        <input type="submit" class="boton rojo derecha" value="Enviar" />
                    </li>
                </ul>
            </form>
        </div>
        <div class="col-md-6 col-lg-7">
            <!-- Espacio para la lista de vacantes -->
            
       <h3 class="titulo">Vacantes actuales</h3><br />
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>Auxiliar administrativo contable en Bogotá, D.C.</strong>
            </div>
            <div>
                <strong><br />
                Salario: </strong>
$1.100.000 (Neto mensual)
                  <br />
            <strong>
Descripción: </strong>
Se requiere Auxiliar Administrativo(a) con Formación académica del SENA en contabilidad y/o carreras afines. 
Experiencia en facturación, manejo de caja, pagos con tarjetas de crédito, con cheque, recaudo dineros, manejo de inventario, manejo de programa contable CG UNO O preferiblemente UNO ENTERPRISE, manejo de Excel, Word, Internet y funciones administrativas<br />
<br />

<strong>Fecha de contratación:</strong> 01/02/2016

 

<br />
<strong>Cantidad de vacantes:</strong> 1<br />
<strong> <br />
Requerimientos:</strong>

                 

         <br />
         Experiencia mínima de 2 años en el cargo.

         

         <br />
         Horario Lunes a Viernes de 7:30 Am a 5:00 Pm (1 hora de almuerzo).
         <br />
         Sábados 8:00 Am a 1:00 Pm </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>Auxiliar de Bodega - Sector Ferreterro y/o Construcción liviana en Bogotá, D.C</strong>
            </div>
            <div>
                <strong><br />
                Salario: </strong>
$689.455 (Neto mensual)
              <br />
            <strong>
Descripción: </strong>
Se requiere Auxiliar de Bodega con experiencia mínima de 1 año en el mismo cargo. Preferiblemente con conocimientos en Logística (SENA).

Entre sus funciones: Cargar la mercancía en los camiones de despacho, según las normas establecidas para el manejo, almacenamiento, preservación y entrega, de los productos comercializados por la compañía.<br />
<br />
Conocimientos en productos de ferretería, Persona proactiva, con compromiso y sentido de pertenecía. Actitud de servicio al cliente.

<br />
<br />
<strong>Fecha de contratación:</strong> 01/02/2016<br />
<strong>Cantidad de vacantes:</strong> 2<br />
<strong> <br />
Requerimientos:</strong>

         <br />
         Estudios requeridos: Bachiller

         <br />
         Años de experiencia: 1

         
          </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>Asesora comercial - Productos de decoración en Valle del Cauca </strong>
            </div>
            <div>
              <strong><br />
              Salario: </strong>
$1.300.000 (Neto mensual)
                  <br />
                  <br />
                  <strong>Descripción:</strong> Empresa líder en la comercialización de productos de decoración, requiere Mujer para el cargo en mención. Para el área de ventas, experiencia en ventas y/o servicio al cliente mínimo de (1) un año.<br />
                  
<br />
<strong>Estudios:</strong> Diseño de Interiores, dibujante arquitectónico y/o carreras afines. Conocimientos de temas de iluminación, habilidad de negociación, perfil comercial. <br />
              <br />
              Manejo de excel, preferiblemente conocimiento del programa UNO Enterprise.

              <br />
              <br />
              <strong>Indispensable: Debe poseer vehículo o moto. </strong><br />
<br />
              <strong>Fecha de contratación:</strong> 01/06/2016

 

<br />
<strong>Cantidad de vacantes:</strong>1<br />

            </div>
            
            
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>Conductor con experiencia - Vehiculo 5 Toneladas Contruccion Liviana en Valle del Cauca</strong>
            </div>
            <div>
              <strong><br />
              Salario: </strong>
$808.000 (Neto mensual)
<br />
                  <br />Empresa importante del sector de la construcción requiere Conductor de Camión Diesel, NPR de 5 toneladas, con licencia de conducción Categoría C2, experiencia certificada mínima de (3) años en el manejo de este tipo de vehículo. 

Conocimiento amplio en nomenclatura en la ciudad , entre sus funciones también se hace cargue, indispensable tener disposición para esta labor . 

<br />
              <br>
              <strong>Experiencia:</strong> En pedidos de contra- entrega. Buenas relaciones interpersonales con los clientes.
              <br>
              <br><strong>Experiencia laboral:</strong> Mas de 3 años de Experiencia conduciendo camiones o buses intermunicipales. NO conductor de buseta o colectivo, busetas o transporte URBANO. o Conductor de Transportadora de Mercancías.
              <strong>No debe tener ninguna clase de reportes, multas, comparendos, etc. Excelente Historial.</strong><br>
              <br><strong>Competencias:</strong> Proactividad, puntualidad y orientación al logro, Confiable, Respetuoso, tolerante al trabajo bajo presión, conocimiento de rutas, experiencia en conducción de vehículo pesado, Disponibilidad, Colaborador. <br>
              <br />
              <strong>Fecha de contratación:</strong> 21/03/2016

 

<br />
<strong>Cantidad de vacantes:</strong> 1<br />
 
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>Vigía Sisoma - Construccion Liviana Sistema Drywall en Bogotá, D.C</strong>
            </div>
            <div>
              <strong><br />
              Salario: </strong>
$750.000 (Neto mensual)
<br />
                  <br />Empresa del Sector de la construcción liviana requiere para uno de sus proyectos en la ciudad de Bogotá, Vigía Sisoma, con formación en Salud Ocupacional. Preferiblemente con certificado de trabajo en alturas. 
<br />
              <br>
              <strong>Experiencia en el cargo mínima de 6 meses.</strong> 
              <br>
              <br />
              <strong>Entre sus funciones:</strong> Realizar informes de gestión de uso de epps.
Capacitaciones, charlas de 5 minutos, 
Señalizacion de la obra, área de trabajo.
Actualización de la carpeta de contratistas, 
Entre otras. <br />
              <br><strong>Requisitos:<br>
              </strong>Tecnóloga en contaduría, administración o carreras afines.
              <br>
              Indispensable manejo del programa contable UNO E ENTERPRISE O CGUNO<br>
Experiencia mínima de 2 años en el cargo<br>Persona proactiva, manejo excel, orientación al servicio al cliente, trabajo en equipo.
<br>
              
              <br />
              <strong>Fecha de contratación:</strong> 21/03/2016

 

<br />
<strong>Cantidad de vacantes:</strong> 1<br />

            </div>
            
            
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>Oficios Varios - Construccion Liviana Sistema Drywall en Bogotá, D.C.</strong>
            </div>
            <div>
              <strong><br />
              Salario: </strong>
$689.455 (Neto mensual)
<br />
                  <br />Empresa del Sector de la Construcción Liviana, requiere para una de sus Obras en la ciudad de Bogotá, Personal preferiblemente masculino, para labor de oficios varios, que se encargue de las siguientes funciones:<br />
                  <br />
              
           - Recoger todos los sobrantes de obra y llevarlos al centro de acopio.
           <br />
           - Realizar jornada de orden y aseo diario en los puestos de trabajo. 
           <br />
           - Reciclar el material sobrante de perfilería y devolverlo a la bodega. 
           <br />
           - Coordinar la disposición final de los residuos, barrer, organizar el material que se está utilizando en obra. 
           <br />
           - Barrer y trapear. <br />
           <br />
           <strong>Requisitos:</strong> <br />
           Bachiller<br />
           persona pro activa. <br />
           <br />
              <strong>Fecha de contratación:</strong> 21/03/2016

 

<br />
<strong>Cantidad de vacantes:</strong> 2<br />
            </div>
            
            
            
        </div>
        
        
        </div>
    </div>
    
    
</div>
<br />
<br />