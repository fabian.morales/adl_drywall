<div id="inline_camp" class="container" style="display: none; min-width: 320px; min-height: 300px; overflow-x: hidden;">
    <form name="form_camp" id="form_camp" action="{$base_dir}index.php?controller=custom&tarea=camp" method="post">
        <input type="hidden" id="vista" name="vista" value="respuesta_form" />
        <div class="row text-center">
            <div class="col-sm-12">
                <h1>Bienvenido a nuestra tienda virtual</h1>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-sm-12">
                Registre sus datos y reciba una camiseta 100% algod&oacute;n
                <hr />
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <label for="nombre_camp">Nombre y apellidos</label>
            </div>
            <div class="col-md-3">
                <input type="text" id="nombre_camp" name="nombre_camp" />
            </div>
            <div class="col-md-3">
                <label for="celular_camp">Celular</label>
            </div>
            <div class="col-md-3">
                <input type="text" id="celular_camp" name="celular_camp" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-3">
                <label for="cedula_camp">C&eacute;dula / Nit</label>
            </div>
            <div class="col-md-3">
                <input type="text" id="cedula_camp" name="cedula_camp" />
            </div>
            <div class="col-md-3">
                <label for="ocupacion_camp">Ocupaci&oacute;n</label>
            </div>
            <div class="col-md-3">
                <input type="text" id="ocupacion_camp" name="ocupacion_camp" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-3">
                <label for="email_camp">Correo electr&oacute;nico</label>
            </div>
            <div class="col-md-3">
                <input type="text" id="email_camp" name="email_camp" />
            </div>
            <div class="col-md-3">
                <label for="ciudad_camp">Ciudad</label>
            </div>
            <div class="col-md-3">
                <input type="text" id="ciudad_camp" name="ciudad_camp" />
            </div>
        </div>
        <br />        
        <div class="row">
            <div class="col-md-12">
                <hr />
                &iquest;Qu&eacute; servicios adicionales quisiera recibir usted de ADL Drywall?
                <br />
                <ul>
                    <li><label for="pedidos_internet"><input type="radio" id="pedidos_internet" name="servicios_adicionales" value="Pedidos por internet" checked="checked"> Pedidos por internet</label></li>
                    <li><label for="app_movil"><input type="radio" id="app_movil" name="servicios_adicionales" value="App Movil para despiece de materiales"> App M&oacute;vil para despiece de materiales</label></li>
                    <li><label for="asesoria_planeacion"><input type="radio" id="asesoria_planeacion" name="servicios_adicionales" value="Asesoria en planeación de obras"> Asesor&iacute;a en planeación de obras</label></li>
                    <li><label for="otros_serv"><input type="radio" id="otros_serv" name="servicios_adicionales" value="Otros"> Otros.</label><div id="div_otros_serv" class="hidden">&nbsp;&iquest;Cu&aacute;l?&nbsp;<input type="text" name="otro_servicio" id="otro_servicio" /></div></li>
                </ul>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-12">
                <input type="submit" value="Enviar" class="boton negro right" id="btnEnviarCamp" />
            </div>
        </div>
    </form>
</div>