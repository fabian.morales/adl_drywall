{capture name=path}Videos - {$cat_video}{/capture}
<div class="container">
    <div class="row">
        <div class="col-md-4 col-lg-3">
            <div class="titulo-lista-cat">Videos - {$cat_video}</div>
            <ul class="lista-cat">
                {foreach from=$cat_videos key=index item=cat}
                <li><a href="{$base_dir}index.php?controller=custom&vista=videos&categoria={$index}">{$cat}</a></li>
                {/foreach}
            </ul>            
        </div>
        <div class="col-md-8 col-lg-9">
            <h1 class="titulo">{$cat_video}</h1>
            <div class="row">
                {foreach from=$videos item=video}
                <div class="col-sm-12 col-md-4">
                    <div class="obras video">
                        <iframe class="iframe_video" src="{$video.url}" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <span>{$video.descripcion}</span>
                </div>
                {/foreach}
            </div>
        </div>
    </div>
</div>
<br />
<br />