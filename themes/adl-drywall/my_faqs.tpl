<script>
(function($, window){
    $(document).ready(function() {        
    	$("div.boton_faq").click(function() {
            if ($(this).parent().hasClass('activo')){
                $(".acordeon.activo").removeClass('activo');
            }
            else{
                $(".acordeon.activo").removeClass('activo');
                $(this).parent().addClass('activo');   
            }
        });
    });
})(jQuery, window);    
</script>
{capture name=path}Preguntas Frecuentes{/capture}
<h1 class="titulo">Preguntas Frecuentes</h1>
<br />
<div class="acordeon activo">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Cómo colgar objetos sobre paredes drywall?</strong>
</div>
<div>
   Existen diferentes tipos de fijaciones para paredes construidas con placas de yeso. Antes de realizar la fijación de un objeto sobre la pared, deberá tener en cuenta su peso para poder definir el tipo y cantidad de anclajes a utilizar, si será necesario detectar la ubicación de la estructura o realizar refuerzos en la estructura de la pared. 
      <br />
      <br />
No utilice puntillas. Se recomiendan los tornillos Mariposa o fijadores de muros huecos que se instalan únicamente en superficies de yeso sobre metal desplegado con una resistencia al desprendimiento permitido de 20lb . Se debe tener en cuenta que cuando el tornillo se quita, la mariposa de la parte posterior se cae y queda en el hueco del muro; por otro lado, es necesario realizar una perforación grande para permitir que las alas de la mariposa pasen por las caras del muro.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Cómo debe realizar el tratamiento de juntas entre placas?</strong>
</div>
<div>
    Para realizar el tratamiento de juntas entre placas, se deberá utilizar Masilla y cinta de papel de celulosa especial microperforada y premarcada en el
    centro. La masilla podrá ser del tipo Lista Para Usar ó de Secado Rápido. Antes de comenzar, se deberá verificar que las superficies a unir estén limpias y
    libres de polvo.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Cómo se debe preparar la superficie antes de pintar?</strong>
</div>
<div>
    En aquellas zonas donde sea necesario, se podrá lijar suavemente con lija fina al agua, procurando no dañar el papel de la superficie de la placa ni la
    cinta de papel de las juntas. Para el acabado con pinturas vinilo, se recomienda la aplicación de una primera mano de sellador, no siendo necesario
    realizar el enduido total de la superficie. Si se aplicarán pinturas satinadas, sintéticas o epoxi, al igual que sí la superficie recibirá iluminación
    rasante, se recomienda realizar un masillado total con Masilla Gyplac, aplicándola en dos manos, como estucado de toda la superficie. Una vez realizado
    este masillado total, se aplicará el sellador recomendado para el tipo de pintura elegido.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Cómo se deben cortar las placas?</strong>
</div>
<div>
    Las placas Gyplac se pueden cortar a la medida que se desee, utilizando para ello un bisturí o cuchillo cartonero.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Qué características tienen las placas Exsound y para qué se utilizan?</strong>
</div>
<div>
    Las Placas Exsound son placas de yeso con perforaciones cuadradas o circulares con características fonoabsorbentes y estéticas. Están revestidas en su cara
    posterior con un velo de fibra de vidrio que reduce la reverberación y crea una barrera contra el polvo y partículas.
    <br />
    Se utilizan para construir paredes y revestimientos en áreas no expuestas a impactos y cielo rasos suspendidos, permitiendo:
    <ul>
        <li>
            El control de la absorción acústica y de la reverberación de los sonidos (NRC de hasta 0.73). Soluciones estéticas y de diseño.
        </li>
        <li>
            Superficies de excelente calidad de terminación.
        </li>
        <li>
            Instalación simple, rápida y limpia.
        </li>
        <li>
            Una barrera contra el polvo y partículas, gracias al velo de fibra de vidrio adherido al reverso de la placa.
        </li>
    </ul>    
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Se puede resolver un problema de humedad con placas Gyplac?</strong>
</div>
<div>
    Por tratarse de un material cuyo núcleo está compuesto por yeso, las placas Gyplac no son aptas para resolver problemas de humedad. Toda superficie que
    será revestida con placas Gyplac, ya se trate de placa Estándar, Resistente a la Humedad o al Fuego, deberá estar libre de humedad. En caso de realizar el
    revestimiento de una pared en estas condiciones, se resolverá primero el origen de la humedad y una vez que la pared esté seca, se podrá realizar el
    revestimiento con placas Gyplac.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Se puede revestir con cerámicos una pared Gyplac?</strong>
</div>
<div>
    Son aptas para recibir cualquier tipo de terminación. Para realizar un revestimiento cerámico se utilizará adhesivo cementicio no siendo necesario
    humedecer previamente la superficie. Si bien la superficie recibirá un revestimiento, se deberá realizar el tomado de juntas entre placas con Masilla
    Gyplac, cinta de papel micro perforado y una capa de masilla.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Se pueden utilizar las placas Gyplac Resistentes a la Humedad en semicubiertos o exteriores?</strong>
</div>
<div>
    Debido a la utilización de componentes hidrofugantes en su fabricación, la Placa Gyplac Resistente a la Humedad tiene mayor resistencia a la humedad que la
    Placa Estándar. No obstante, el papel que recubre la placa, si bien es de color verde (la cara vista) es el mismo tipo de papel con el que se fabrica la
    placa estándar por tal motivo, su utilización en exteriores o semicubiertos no es recomendable, porque podría presentar posteriores deterioros debidos a la
    exposición de la misma a la intemperie. Para estos casos se deberán utilizar placas cementicias Superboard.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Qué tipo de estructura se utiliza en cielos rasos?</strong>
</div>
<div>
    Para construir cielo rasos, se deberá armar una estructura de perfiles de lámina de acero galvanizada por inmersión en caliente, conformada en frío, sobre
    la cual se fijarán las placas. Se pueden utilizar perfiles omegas y perimetrales o parales y canales de 40 mm, siguiendo las indicaciones del MTG,
    colocando a la distancia correspondiente las cuelgas y las respectivas vigas principales.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Por qué las tejas sin asbesto se humeden más que las de asbesto?</strong>
</div>
<div>
    Por su tono oscuro pareciera que se humedece más que la de la competencia, pero todas las tejas que tiene cemento presentan humedecimiento, hasta que se
    cierren sus poros.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Por qué las tejas gotean y me dicen que no están malas?</strong>
</div>
<div>
    Las tejas como lo dice la norma NTC 4694 pueden presentar humedecimiento, más no goteo. Se debe hacer la prueba de permeabilidad para comprobar el estado
    de la teja, si ésta arroja como resultado goteo, se deben cambiar.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Cuál es la duración de una teja?</strong>
</div>
<div>
    Tenemos indicios de tejas de fibrocemento hasta de 50 años.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Cuál es la garantía de una teja?</strong>
</div>
<div>
    La garantía por defecto de producción es de 5 años.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Las placas de fibrocemento de 6mm sirven para fachadas?</strong>
</div>
<div>
    No sirven para exteriores; se recomienda de 10 mm en adelante. Por su estabilidad y seguridad, además por los niveles climáticos a los que está expuesta
    una fachada, los vientos y demás fenómenos que se generan a la intemperie.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Se pueden utilizar dos placas de fibrocemento de 10mm en un entrepiso?</strong>
</div>
<div>
    No se puede. Ambas placas tendrían diferentes movimientos y eso ocasionaría una inestabilidad en el entrepiso, además de los daños que ocasionaría en sí el
    acabado final.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Puedo instalar Superboard para una chimenea – horno?</strong>
</div>
<div>
    No se recomienda, la placa no debe estar expuesta a temperaturas mayores a 60°C.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Puedo instalar Superboard para un cuarto frío?</strong>
</div>
<div>
    Sí se puede. Se recomienda que las juntas sean flexibles y que se proteja la placa por todos sus cantos con impermeabilizante; en caso de no utilizar
    cerámicas, se debe utilizar pinturas epóxicas.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Puedo instalar Superboard para un cuarto rayos X?</strong>
</div>
<div>
    Sí se puede. Se recomienda hacer dos medias paredes y en medio de estos perfiles instalar la placa de plomo.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Puedo utilizar Superboard como cubierta?</strong>
</div>
<div>
    Sí se puede instalar. Hay que tener una mínima pendiente e impermeabilizar la placa con mantos asfálticos, pinturas impermeabilizantes, membranas flexibles
    de PVC o cualquier tipo de teja.
</div>

<div class="acordeon">
    <div class="boton_faq">&nbsp;</div>
    <strong>¿Qúe es UECEB?</strong>
</div>
<div>
    EUCEB (European Certification Board) Es una certificación internacional otorgada únicamente a FiberGlass en Latinoamérica. Garantiza que la lana mineral de
    vidrio empleada para la fabricación de los productos FiberGlass es segura para la salud.
</div>