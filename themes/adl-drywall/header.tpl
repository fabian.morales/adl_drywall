{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="{$lang_iso}"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="{$lang_iso}"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="{$lang_iso}"><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="{$lang_iso}"><![endif]-->
<html lang="{$lang_iso}">
    <head>
        <meta charset="utf-8" />
        <title>{$meta_title|escape:'html':'UTF-8'}</title>
{if isset($meta_description) AND $meta_description}
        <meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
{/if}
{if isset($meta_keywords) AND $meta_keywords}
        <meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
{/if}
        <meta name="generator" content="PrestaShop" />
        <meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
        <meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" /> 
        <meta name="apple-mobile-web-app-capable" content="yes" /> 
        <link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
        <link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
{if isset($css_files)}
	{foreach from=$css_files key=css_uri item=media}
        <link rel="stylesheet" href="{$css_uri}" type="text/css" media="{$media}" />
	{/foreach}
{/if}
{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
	{$js_def}
	{foreach from=$js_files item=js_uri}
	<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
	{/foreach}
{/if}
    <link rel="stylesheet" href="{$css_dir}style.css" type="text/css" media="{$media}" />
    <script type="text/javascript" src="{$js_dir}my.js"></script>
        {$HOOK_HEADER}
        <link rel="stylesheet" href="http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Open+Sans:300,600" type="text/css" media="all" />
        <!--[if IE 8]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        {*if $page_name == 'index'}
            <script>
                (function($, window){        
                    $(document).ready(function() {                        
                        $.fancybox({
                            'href': '#inline_camp',
                            'autoSize': true,
                            'autoHeight': true,
                            'autoWidth': true,
                            'transitionIn': 'elastic',
                            'transitionOut': 'elastic',
                            'speedIn': 500,
                            'speedOut': 300,
                            'autoDimensions': true,
                            'centerOnScroll': true,
                            'afterLoad': function() {
                                $("input[type=radio][name=servicios_adicionales]").change(function() {
                                    if ($(this).val() === 'Otros' && $(this).is(":checked")){
                                        $("#div_otros_serv").removeClass("hidden");
                                    }
                                    else{
                                        $("#otro_servicio").val('');
                                        $("#div_otros_serv").addClass("hidden");
                                    }
                                });
                                
                                $("#btnEnviarCamp").click(function(e) {
                                    e.preventDefault();
                                    
                                    if ($("#nombre_camp").val() === ''){
                                        alert('Debe ingresar su nombre y apellido');
                                        return;
                                    }
                                    
                                    if ($("#celular_camp").val() === ''){
                                        alert('Debe ingresar su numero de celular');
                                        return;
                                    }
                                    
                                    if ($("#cedula_camp").val() === ''){
                                        alert('Debe ingresar su numero de cedula o nit');
                                        return;
                                    }
                                    
                                    if ($("#ocupacion_camp").val() === ''){
                                        alert('Debe ingresar su ocupacion');
                                        return;
                                    }
                                    
                                    if ($("#email_camp").val() === ''){
                                        alert('Debe ingresar su direccion de correo electronico');
                                        return;
                                    }
                                    
                                    if ($("#ciudad_camp").val() === ''){
                                        alert('Debe ingresar la ciudad');
                                        return;
                                    }
                                    
                                    if ($("#otros_serv").is(":checked") && $("#otro_servicio").val() === ''){
                                        alert('Debe ingresar el servicio adicional');
                                        return;
                                    }
                                    
                                    $("#form_camp").submit();
                                });
                            }
                        });
                    });
                })(jQuery, window);
            </script>
        {/if*}
    </head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{/if}{if $hide_right_column} hide-right-column{/if}{if $content_only} content_only{/if} lang_{$lang_iso}">
	{if !$content_only}
            {if isset($restricted_country_mode) && $restricted_country_mode}
                <div id="restricted-country">
                    <p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country}</span></p>
                </div>
            {/if}
            <div id="page">
                <div class="header-container">
                    <header id="header">
                        <div class="banner">
                            <div class="container">
                                <div class="row">
                                    {hook h="displayBanner"}
                                </div>
                            </div>
                        </div>
                            <div class="nav">
                                <div class="container">
                                    <div class="row">
                                        <nav>{hook h="displayNav"}</nav>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="container">
                                    <div class="row">
                                        <div id="header_logo" class="col-sm-4">
                                            <a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
                                                <img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if $logo_image_width} width="{$logo_image_width}"{/if}{if $logo_image_height} height="{$logo_image_height}"{/if}/>
                                            </a>
                                        </div>
                                        <div class="col-sm-2">&nbsp;</div>
                                        <div class="col-sm-6 row-top">
                                            <div class="row">
                                                {if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            {hook h="displayTopMenu"}
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </header>
                </div>
                <div class="columns-container">
                    <div id="columns" class="container">
                        {if $page_name !='index' && $page_name !='pagenotfound'}
                            {include file="$tpl_dir./breadcrumb.tpl"}
                        {/if}
                        <div class="row">
                            <div id="top_column" class="center_column col-xs-12 col-sm-12 no-padding">{hook h="displayTopColumn"}</div>
                        </div>
                        <div class="row">
                            {if isset($left_column_size) && !empty($left_column_size)}
                            <div id="left_column" class="column col-xs-12 col-sm-{$left_column_size|intval}">{$HOOK_LEFT_COLUMN}</div>
                            {/if}
                            <div id="center_column" class="center_column col-xs-12 col-sm-{12 - $left_column_size - $right_column_size}">
	{/if}