<script>
(function($, window){
    $(document).ready(function() {
        $('#galeria').lightSlider({
            gallery:true,
            item:1,
            loop:true,
            thumbItem:5,
            slideMargin:0,
            enableDrag: true,
            currentPagerPosition:'left',
            controls: true,
            onSliderLoad: function(el) {
                el.lightGallery({
                    selector: '#galeria .lslide'
                });
            }   
        });
    });
})(jQuery, window);    
</script>
{capture name=path}Capacitaciones{/capture}
<h1 class="text-center titulo">Ahorra costos y tiempo en obra con el sistema liviano - Gyplac</h1>
<div class="row row-centered">
    <div class="col-md-6 col-centered">        
        <ul id="galeria">
            <li data-thumb="{$base_dir}/imagenes/capacitaciones4/Adl_Drywall_Capacitacion_Gyplac_01.jpg" data-src="{$base_dir}/imagenes/capacitaciones4/Adl_Drywall_Capacitacion_Gyplac_01.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones4/Adl_Drywall_Capacitacion_Gyplac_01.jpg" />
            </li>
            <li data-thumb="{$base_dir}/imagenes/capacitaciones4/Adl_Drywall_Capacitacion_Gyplac_02.jpg" data-src="{$base_dir}/imagenes/capacitaciones4/Adl_Drywall_Capacitacion_Gyplac_02.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones4/Adl_Drywall_Capacitacion_Gyplac_02.jpg" />
            </li>
            <li data-thumb="{$base_dir}/imagenes/capacitaciones4/Adl_Drywall_Capacitacion_Gyplac_03.jpg" data-src="{$base_dir}/imagenes/capacitaciones4/Adl_Drywall_Capacitacion_Gyplac_03.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones4/Adl_Drywall_Capacitacion_Gyplac_03.jpg" />
            </li>         
        </ul>
    </div>
</div>
<br />
<br />
<br />
