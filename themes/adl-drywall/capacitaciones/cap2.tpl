<script>
(function($, window){
    $(document).ready(function() {
        $('#galeria').lightSlider({
            gallery:true,
            item:1,
            loop:true,
            thumbItem:5,
            slideMargin:0,
            enableDrag: true,
            currentPagerPosition:'left',
            controls: true,
            onSliderLoad: function(el) {
                el.lightGallery({
                    selector: '#galeria .lslide'
                });
            }   
        });
    });
})(jQuery, window);    
</script>
{capture name=path}Capacitaciones{/capture}
<h1 class="text-center titulo">2do curso de Actualización e instalación</h1>
<div class="row row-centered">
    <div class="col-md-6 col-centered">        
        <ul id="galeria">
            <li data-thumb="{$base_dir}/imagenes/capacitaciones2/01.jpg" data-src="{$base_dir}/imagenes/capacitaciones2/01.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones2/01.jpg" />
            </li>
            <li data-thumb="{$base_dir}/imagenes/capacitaciones2/02.jpg" data-src="{$base_dir}/imagenes/capacitaciones2/02.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones2/02.jpg" />
            </li>
            <li data-thumb="{$base_dir}/imagenes/capacitaciones2/03.jpg" data-src="{$base_dir}/imagenes/capacitaciones2/03.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones2/03.jpg" />
            </li>
            <li data-thumb="{$base_dir}/imagenes/capacitaciones2/04.jpg" data-src="{$base_dir}/imagenes/capacitaciones2/04.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones2/04.jpg" />
            </li>
            <li data-thumb="{$base_dir}/imagenes/capacitaciones2/05.jpg" data-src="{$base_dir}/imagenes/capacitaciones2/05.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones2/05.jpg" />
            </li>
        </ul>
    </div>
</div>
<br />
<br />
<br />
