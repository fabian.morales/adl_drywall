<script>
(function($, window){
    $(document).ready(function() {
        $('#galeria').lightSlider({
            gallery:true,
            item:1,
            loop:true,
            thumbItem:5,
            slideMargin:0,
            enableDrag: true,
            currentPagerPosition:'left',
            controls: true,
            onSliderLoad: function(el) {
                el.lightGallery({
                    selector: '#galeria .lslide'
                });
            }   
        });
    });
})(jQuery, window);    
</script>

{capture name=path}Capacitaciones{/capture}
<h1 class="text-center titulo">Capacitación Tejas Skinco - Sena</h1>
<div class="row row-centered">
    <div class="col-md-6 col-centered">        
        <ul id="galeria">
            <li data-thumb="{$base_dir}/imagenes/capacitaciones3/01.jpg" data-src="{$base_dir}/imagenes/capacitaciones3/01.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones3/01.jpg" />
            </li>
            <li data-thumb="{$base_dir}/imagenes/capacitaciones3/02.jpg" data-src="{$base_dir}/imagenes/capacitaciones3/02.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones3/02.jpg" />
            </li>
            <li data-thumb="{$base_dir}/imagenes/capacitaciones3/03.jpg" data-src="{$base_dir}/imagenes/capacitaciones3/03.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones3/03.jpg" />
            </li>
            <li data-thumb="{$base_dir}/imagenes/capacitaciones3/04.jpg" data-src="{$base_dir}/imagenes/capacitaciones3/04.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones3/04.jpg" />
            </li>
            <li data-thumb="{$base_dir}/imagenes/capacitaciones3/05.jpg" data-src="{$base_dir}/imagenes/capacitaciones3/05.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones3/05.jpg" />
            </li>
            <li data-thumb="{$base_dir}/imagenes/capacitaciones3/06.jpg" data-src="{$base_dir}/imagenes/capacitaciones3/06.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones3/06.jpg" />
            </li>
            <li data-thumb="{$base_dir}/imagenes/capacitaciones3/07.jpg" data-src="{$base_dir}/imagenes/capacitaciones3/07.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones3/07.jpg" />
            </li>
            <li data-thumb="{$base_dir}/imagenes/capacitaciones3/08.jpg" data-src="{$base_dir}/imagenes/capacitaciones3/08.jpg">
                <img src="{$base_dir}/imagenes/capacitaciones3/08.jpg" />
            </li>
        </ul>
    </div>
</div>
<br />
<br />
<br />
