{capture name=path}Bogotá Norte{/capture}
{extends file='./sedes.tpl'}
{block name='contenido_sede'}
    <div class="titulo-categorias">Bogotá Norte</div>
    <div class="row">
        <div class="col-md-7">          
            <p><strong>Dirección: </strong>Calle 143 # 45 - 57 </p>
            <p><strong>Teléfono: </strong> 742 1010</p>
            <br />
            <h3>Equipo de trabajo</h3>
            <ul class="row personal">
                <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/bogota3/Asesor_01_Yenny_Moreno.jpg" class="img-responsive" /></li>
                <li class="col-md-9"><strong>Yenni Maribel Moreno:</strong> "Yo soy la EFICIENTE, estoy siempre dispuesta a colaborar y brindarle solución a sus necesidades de manera oportuna."</li>

                <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/bogota3/Asesor_02_Monica_Florian.jpg" class="img-responsive" /></li>
                <li class="col-md-9"><strong>Mónica Patricia Florián:</strong> "Yo soy RESPONSABLE y estoy para colaborarles con el mayor de los gustos."</li>
            </ul>
        </div>
        <div class="col-md-5">
            <img src="{$base_dir}imagenes/sedes/bogota3/Foto_Grupal_Capital_Norte.jpg" class="img-responsive" />
        </div>
    </div>    

    <ul class="row personal left0">
        <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/bogota3/Asesor_04_Sandra_Florian.jpg" class="img-responsive" /></li>
        <li class="col-md-3 col-lg-4"><strong>Sandra Marcela Florián:</strong> "Yo soy COLABORADORA y estoy para brindarle diferentes alternativas a nuestros clientes."</li>

        <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/bogota3/Asesor_03_Pablo_Zabala.jpg" class="img-responsive" /></li>
        <li class="col-md-3 col-lg-4"><strong>Pablo Alexander Zabala:</strong> "Yo soy el SABIO y estoy para brindarle soluciones oportunas a todos sus requerimientos."</li>
    </ul>
{/block}