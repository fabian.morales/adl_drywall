{capture name=path}Bogotá 1{/capture}
{extends file='./sedes.tpl'}
{block name='contenido_sede'}
    <div class="titulo-categorias">Bogotá Paloquemao</div>
    <div class="row">
        <div class="col-md-7">
            <p><strong>Dirección:</strong> Calle 17 No. 28A – 45 Paloquemao</p>
            <p><strong>Teléfono:</strong> 744 6213</p>
            <p><strong>Fax:</strong> 277 4621</p>
            <br />
            <h3>Equipo de trabajo</h3>
            <ul class="row personal">
                <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/bogota1/Asesor_01.jpg" class="img-responsive" /></li>
                <li class="col-md-9"><strong>Alvaro Gonzales:</strong> "Mi nombre es Alvaro Gonzalez gerente comercial de la regional Bogotá. Me apasiona el trabajo en equipo y por eso usted es parte de nuestro equipo."</li>

                <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/bogota1/Asesor_03_Susan.jpg" class="img-responsive" /></li>
                <li class="col-md-9"><strong>Susan Ibañez:</strong> "Me caracterizo por hacer las cosas con amor, dedicación, entusiasmo y gran pasión por las ventas."</li>
            </ul>        
        </div>
        <div class="col-md-5">
            <img src="{$base_dir}imagenes/sedes/bogota1/Foto_Grupal_Paloquemao.jpg" class="img-responsive" />
        </div>
    </div>    
    <ul class="row personal left0">
        <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/bogota1/Asesor_05_Mari.jpg" class="img-responsive" /></li>
        <li class="col-md-3 col-lg-4"><strong>Maribel Prieto:</strong> "Estoy comprometida con mi labor y me esmero por dar solución a sus requerimientos."</li>
        
        <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/bogota1/Asesor_04_Kelly.jpg" class="img-responsive" /></li>
        <li class="col-md-3 col-lg-4"><strong>Kelly Castillo:</strong> "Me gusta mucho mi labor como asesora y me encantan las ventas, hago las cosas con dedicación, disciplina y amor."</li>

        <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/bogota1/Asesor_02_Francy.jpg" class="img-responsive" /></li>
        <li class="col-md-3 col-lg-4"><strong>Francy Abril:</strong>"Estoy comprometida con mi labor y me apasionan las ventas."</li>
    </ul>
{/block}