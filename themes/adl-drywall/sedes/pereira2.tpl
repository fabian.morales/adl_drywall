{capture name=path}Pereira Cra 16 bis{/capture}
{extends file='./sedes.tpl'}
{block name='contenido_sede'}
    <div class="titulo-categorias">Pereira Cra 16</div>
    <div class="row">
        <div class="col-md-6">
            <p><strong>Dirección:</strong> Cra. 16 Bis No. 17 -12</p>
            <p><strong>Teléfono:</strong> 340 0071 / 324 4167</p>
            <br />
            <h3>Equipo de trabajo</h3>
            <ul class="row personal">
                <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/pereira2/Asesor_01_Norma.jpg" class="img-responsive" /></li>
                <li class="col-md-9"><strong>Norma Perdomo:</strong> "Mi nombre es Norma Perdomo y me caracterizo por ser una persona con valores y cumplidora de las políticas de la empresa."</li>
                <li class="col-md-3"><img src="{$base_dir}imagenes/usuario.png" class="img-responsive" /></li>
                <li class="col-md-9"><strong>Carlos Vega:</strong> "Durante mi trayectoria por la empresa me he destacado por ser un empleado comprometido y muy versátil, permitiéndome ganar mucha experiencia en el campo comercial y administrativo."</li>     
            </ul>        
        </div>
        <div class="col-md-5">
            <img src="{$base_dir}imagenes/sedes/pereira2/Foto_Grupal_Pereira_Cra_16.jpg" class="img-responsive" />
        </div>
    </div>

   
{/block}