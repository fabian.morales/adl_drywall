{capture name=path}Manizales{/capture}
{extends file='./sedes.tpl'}
{block name='contenido_sede'}
    <div class="titulo-categorias">Manizales</div>
    <div class="row">
        <div class="col-md-7">
            <p><strong>Dirección:</strong> Cra. 25 No. 18 - 32</p>
            <p><strong>Teléfono:</strong> 890 5252 Ext. 6300</p>
            <br />
            <h3>Equipo de trabajo</h3>
            <ul class="row personal">
                <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/manizales/Asesor_01_Sandra.jpg" class="img-responsive" /></li>
                <li class="col-md-9"><strong>Sandra Restrepo:</strong> "Asesora Comercial y Administrativa de la sede Manizales. Soy una persona sencilla, responsable, alegre y disfruto cada día mi trabajo."</li>
            </ul>        
        </div>
        <div class="col-md-5">
            <img src="{$base_dir}imagenes/sedes/manizales/Foto_Grupal_Manizales.jpg" class="img-responsive" />
        </div>
    </div>

    {*<ul class="row personal left0">
        <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/principal/Asesor_02.jpg" class="img-responsive" /></li>
        <li class="col-md-3 col-lg-4"><strong>Claudia Ximena Abondano:</strong> "Mi nombre es CLAUDIA ABONDANO, soy una persona ordenada, respetuosa y estaré siempre con una sonrisa para atenderlo."</li>
        <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/principal/Asesor_04.jpg" class="img-responsive" /></li>
        <li class="col-md-3 col-lg-4"><strong>John Alexander Torres:</strong> "Hola mi nombre es John Alex Torres, soy una  persona proactiva y responsable, cualquier solicitud con mucho gusto será atendida."</li>
        <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/usuario.png" class="img-responsive" /></li>
        <li class="col-md-3 col-lg-4"><strong>Cindy Patricia Santacruz:</strong> "Yo soy Cindy Patricia Santacruz Cárdenas, auxiliar administrativa de la bodega principal, con la mejor actitud para crecer."</li>
    </ul>*}
{/block}