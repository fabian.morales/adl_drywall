{capture name=path}Bogotá 2{/capture}
{extends file='./sedes.tpl'}
{block name='contenido_sede'}
    <div class="titulo-categorias">Bogotá Alquería</div>
    <div class="row">
        <div class="col-md-7">      
            <p><strong>Dirección:</strong> Avenida Carrera 68 Nº 37B-05 Sur</p>
            <p><strong>Teléfono:</strong> 7562336</p>
            <br />
            <br />
            <h3>Equipo de trabajo</h3>
            <ul class="row personal">
                <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/bogota2/Asesor_01_Edith_Johanna_Vanegas.jpg" class="img-responsive" /></li>
                <li class="col-md-9"><strong>Joanna Vanegas:</strong> "Yo soy  Joanna Vanegas, me caracterizo por ser servicial y colaboradora."</li>

                <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/bogota2/Asesor_02_Jennifer_Rodriguez.jpg" class="img-responsive" /></li>
                <li class="col-md-9"><strong>Jennifer Rodriguez:</strong> "Mi nombre es Jennifer Rodriguez, soy optimista y me gustan los retos."</li>
            </ul>        
        </div>
        <div class="col-md-5">
            <img src="{$base_dir}imagenes/sedes/bogota2/Foto_Grupal_Alqueria.jpg" class="img-responsive" />
        </div>
    </div>    
    <ul class="row personal left0">
        <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/bogota2/Asesor_03_Pilar_Callejas.jpg" class="img-responsive" /></li>
        <li class="col-md-3 col-lg-4"><strong>Pilar Callejas:</strong> "Mi nombre es Pilar Callejas, soy generosa y estoy a su disposición para servile."</li>
    </ul>
{/block}