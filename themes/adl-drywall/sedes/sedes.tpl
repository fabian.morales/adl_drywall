{capture name=path}Nuestras sedes{/capture}
<div class="container">
    <div class="row">
        <div class="col-md-4 col-lg-3">
            <div class="titulo-lista-cat">Nuestras Sedes</div>
            <ul class="lista-cat">
                <li><a href="{$base_dir}index.php?controller=custom&vista=sede_ppal">Cali Principal</a></li>
                <li><a href="{$base_dir}index.php?controller=custom&vista=sede_cali_centro">Cali Cra 8</a></li>
                <li><a href="{$base_dir}index.php?controller=custom&vista=sede_cali_sur">Cali Pasoancho</a></li>
                <li><a href="{$base_dir}index.php?controller=custom&vista=bogota1">Bogotá Paloquemao</a></li>
                <li><a href="{$base_dir}index.php?controller=custom&vista=bogota2">Bogotá Alquería</a></li>
                <li><a href="{$base_dir}index.php?controller=custom&vista=bogota3">Bogotá Norte</a></li>
                <li><a href="{$base_dir}index.php?controller=custom&vista=pereira1">Pereira Cra 9Bis</a></li>
                <li><a href="{$base_dir}index.php?controller=custom&vista=pereira2">Pereira Cra 16</a></li>
                <li><a href="{$base_dir}index.php?controller=custom&vista=armenia">Armenia</a></li>
                <li><a href="{$base_dir}index.php?controller=custom&vista=manizales">Manizales</a></li>
                <li><a href="{$base_dir}index.php?controller=custom&vista=pasto">Pasto</a></li>
            </ul>
        </div>
        <div class="col-md-8 col-lg-9">
            {block name='contenido_sede'}{/block}
        </div>
    </div>
</div>
<br />
<br />

