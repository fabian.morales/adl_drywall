{capture name=path}Pereira 9 Bis{/capture}
{extends file='./sedes.tpl'}
{block name='contenido_sede'}
    <div class="titulo-categorias">Pereira Cra 9 Bis</div>
    <div class="row">
        <div class="col-md-7">
            <p><strong>Dirección:</strong> Cra. 9 Bis No. 41 -20</p>
            <p><strong>Teléfono:</strong> 340 1294 - 329 0304</p>
            <br />
            <h3>Equipo de trabajo</h3>
            <ul class="row personal">
                <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/pereira1/Asesor_03_Jose_Omar.jpg" class="img-responsive" /></li>
                <li class="col-md-9"><strong>Jose Omar Cortes:</strong> "Mi nombre es Jose Omar Cortes Sanchez, jefe de ventas del Eje cafetero. Me caracterizo por estar siempre dispuesto para lo que necesiten."</li>

                <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/pereira1/Aseror_ronald.jpg" class="img-responsive" /></li>
                <li class="col-md-9"><strong>Ronald Jaimes:</strong> "Me considero una persona responsable y con gran sentido de pertenencia con la empresa y los clientes. Soy canta autor, compositor de música romántica y popular, con gran inclinación espiritual, amador de la familia, el trabajo y las cosas buenas. Para mí es un placer tener el gusto de servir y atenderle."</li>
            </ul>        
        </div>
        <div class="col-md-5">
            <img src="{$base_dir}imagenes/sedes/pereira1/Foto_Grupal_Pereira_01.jpg" class="img-responsive" />
        </div>
    </div>

        <ul class="row personal left0">
        <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/pereira1/Asesor_01.jpg" class="img-responsive" /></li>
        <li class="col-md-3 col-lg-4"><strong>Gonzalo Montes:</strong> "Mi nombre es Gonzalo Montes, asesor comercial externo, soy una persona honesta y cumplidor de mis deberes."</li>
        
        <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/usuario.png" class="img-responsive" /></li>
        <li class="col-md-3 col-lg-4"><strong>Janier Orlando Arango:</strong> "Mi nombre es Janier Orlando Arango Pineda asesor comercial de la sucursal Pereira Cra 9, soy una persona responsable y cordial."</li>
    </ul>
{/block}