{capture name=path}Sede centro - Cali{/capture}
{extends file='./sedes.tpl'}
{block name='contenido_sede'}
    <div class="titulo-categorias">Cali Cra 8</div>
    <div class="row">
        <div class="col-md-7">
            <p><strong>Dirección:</strong> Carrera 8 No. 23 – 90 San Nicolás</p>
            <p><strong>Teléfono:</strong> 889 0148 - 889 7853</p>
            <br />
            <h3>Equipo de trabajo</h3>
            <ul class="row personal">
                <li class="col-md-2"><img src="{$base_dir}imagenes/sedes/cali_centro/Asesor_01.jpg" class="img-responsive" /></li>
                <li class="col-md-10"><strong>Lina María Zambrano:</strong> "Soy una persona proactiva, con amplio conocimiento en sistemas de construcción liviana, experiencia en atención al cliente, vocación de servicio y sentido de responsabilidad. Siempre dispuesta a brindar lo mejor de mi a la empresa y a los clientes."</li>

                <li class="col-md-2"><img src="{$base_dir}imagenes/sedes/cali_centro/Asesor02_Yuksara.jpg" class="img-responsive" /></li>
                <li class="col-md-10"><strong>Yuksara Getial:</strong> "Soy una persona proactiva, con actitud de servicio y con capacidad de dar respuesta efectiva a las solicitudes de nuestros clientes y nuestra empresa."</li>
            </ul>
        </div>

        <div class="col-md-5">
            <img src="{$base_dir}imagenes/sedes/cali_centro/Foto_Grupal_Cali_Cra_8.jpg" class="img-responsive foto_sedes" />
        </div>
    </div>    
    {*<ul class="row personal left0">    
    </ul>*}
{/block}