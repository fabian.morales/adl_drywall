{capture name=path}Pasto{/capture}
{extends file='./sedes.tpl'}
{block name='contenido_sede'}
    <div class="titulo-categorias">Pasto</div>
    <div class="row">
        <div class="col-md-7">
            <p><strong>Dirección:</strong> Cra. 14 No. 13 - 21</p>
            <p><strong>Teléfono:</strong> 737 4055 - 721 0073</p>
            <br />
            <h3>Equipo de trabajo</h3>
            <ul class="row personal">
                <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/pasto/Asesor_02_Leandro_Revelo.jpg" class="img-responsive" /></li>
                <li class="col-md-9"><strong>Leandro Miguel Revelo:</strong> "¡Hola! Soy Leandro, una persona alegre, cordial y siempre dispuesto a servirle."</li>

                <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/pasto/Asesor_01_Guillermo_Salas.jpg" class="img-responsive" /></li>
                <li class="col-md-9"><strong>Guillermo Orlando Salas:</strong> "Me caracterizo por ser una persona con actitud positiva y con un gran sentido de pertenencia. Siempre dispuesto a satisfacer las necesidades y expectativas de los clientes brindándoles un buen servicio."</li>
            </ul>        
        </div>
        <div class="col-md-5">
            <img src="{$base_dir}imagenes/sedes/pasto/Foto_Grupal_Pasto.jpg" class="img-responsive foto_sedes"/>
        </div>
    </div>

    <ul class="row personal left0">
        <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/pasto/Asesor_04_Yeimy_Leon.jpg" class="img-responsive" /></li>
        <li class="col-md-3 col-lg-4"><strong>Yeimy Lisset León:</strong> "Con actitud positiva, responsable con mi trabajo, atenta, amable y colaboradora. Siempre a su disposición para servirle."</li>
        
        <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/pasto/Asesor_03_William_Cerquera.jpg" class="img-responsive" /></li>
        <li class="col-md-3 col-lg-4"><strong>William Alexander Cerquera:</strong> "Me caracterizo por mi cordialidad, respeto y amabilidad con los clientes, estando siempre a su disposición para servirle."</li>
    </ul>
{/block}