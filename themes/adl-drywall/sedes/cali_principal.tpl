{capture name=path}Sede Principal{/capture}
{extends file='./sedes.tpl'}
{block name='contenido_sede'}
<div class="titulo-categorias">Cali Principal</div>
<div class="row">
    <div class="col-md-7">
        <p>Estamos en el centro de la ciudad de Cali.</p>
        <p><strong>Dirección:</strong> Carrera 4 # 22 - 74 San Nicolás</p>
        <p><strong>Teléfono:</strong> (2) 898 00 33 - 312 555 98 66</p>
        <br />
        <h3>Equipo de trabajo</h3>
        <ul class="row personal">
            <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/principal/Asesor_01.jpg" class="img-responsive" /></li>
            <li class="col-md-9"><strong>Iván Sarria:</strong> "Me describo como una persona con buena disposición, comprometido y ordenado."</li>
            <li class="col-md-3"><img src="{$base_dir}imagenes/sedes/principal/Asesor_03.jpg" class="img-responsive" /></li>
            <li class="col-md-9"><strong>Diana Carolina Velasco:</strong> "Mi nombre es Diana Carolina Velasco asesora comercial de la bodega principal. Me caracterizo por ser una persona proactiva, servicial y alegre."</li>            
        </ul>        
    </div>
    <div class="col-md-5">
        <img src="{$base_dir}imagenes/sedes/principal/Foto_Grupal.jpg" class="img-responsive foto_sedes"/>
    </div>
</div>
<ul class="row personal left0">
    <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/principal/Asesor_02.jpg" class="img-responsive" /></li>
    <li class="col-md-3 col-lg-4"><strong>Claudia Ximena Abondano:</strong> "Mi nombre es Claudia Abondano, soy una persona ordenada, respetuosa y estaré siempre con una sonrisa para atenderlo."</li>
    <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/principal/Asesor_04.jpg" class="img-responsive" /></li>
    <li class="col-md-3 col-lg-4"><strong>John Alexander Torres:</strong> "Hola mi nombre es John Alex Torres, soy una  persona proactiva y responsable, cualquier solicitud con mucho gusto será atendida."</li>
    <li class="col-md-3 col-lg-2"><img src="{$base_dir}imagenes/sedes/principal/Asesor_05.jpg" class="img-responsive" /></li>
    <li class="col-md-3 col-lg-4"><strong>Cindy Patricia Santacruz:</strong> "Yo soy Cindy Patricia Santacruz Cárdenas, auxiliar administrativa de la bodega principal, con la mejor actitud para crecer."</li>
</ul>
{/block}