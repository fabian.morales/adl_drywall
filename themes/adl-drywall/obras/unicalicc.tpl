<script>

(function($, window){

    $(document).ready(function() {

        $('#galeria').lightSlider({

            gallery:true,

            item:1,

            loop:true,

            thumbItem:5,

            slideMargin:0,

            enableDrag: true,

            currentPagerPosition:'left',

            controls: true,

            onSliderLoad: function(el) {

                el.lightGallery({

                    selector: '#galeria .lslide'

                });

            }

        });

        

        $("a[rel='fancy']").fancybox();

    });

})(jQuery, window);    

</script>

{capture name=path}Obras{/capture}

<h1 class="text-center titulo">Cine Colombia Unicentro Cali</h1>

<div class="row row-centered">

    <div class="col-md-6 col-centered">        

        <ul id="galeria">

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/001.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/001.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/001.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/001.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/002.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/002.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/002.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/002.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/003.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/003.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/003.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/003.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/004.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/004.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/004.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/004.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/005.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/005.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/005.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/005.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/006.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/006.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/006.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/006.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/007.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/007.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/007.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/007.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/008.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/008.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/008.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/008.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/009.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/009.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/009.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/009.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/010.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/010.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/010.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/010.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/011.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/011.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/011.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/011.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/012.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/012.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/012.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/012.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/013.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/013.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/013.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/013.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unicalicc/014.jpg" data-src="{$base_dir}/imagenes/obras/unicalicc/014.jpg"><a href="{$base_dir}/imagenes/obras/unicalicc/014.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicalicc/014.jpg" /></a></li>

        </ul>

    </div>

</div>

<br />

<p align="justify"><strong>Obra: </strong> Multiplex Unicentro Cali </p>
<p align="justify"><strong>Uso Edificación: </strong>Comercial, Cines.</p>
<p align="justify"><strong>Ciudad: </strong>Cali</p>
<p align="justify"><strong>Fecha de Inicio: </strong>Noviembre de 2014 - Febrero de 2015</p>
<p align="justify"><strong>Productos: </strong>Construcción de muros acústicos, suministro e instalación del cielo raso del vestíbulo, suministro e instalación de cubierta tipo sandwich deck 525 (Hunter Douglas).</p>

