<script>
(function($, window){
    $(document).ready(function() {
        $('#galeria').lightSlider({
            gallery:true,
            item:1,
            loop:true,
            thumbItem:5,
            slideMargin:0,
            enableDrag: true,
            currentPagerPosition:'left',
            controls: true,
            onSliderLoad: function(el) {
                el.lightGallery({
                    selector: '#galeria .lslide'
                });
            }   
        });
        
        $("a[rel='fancy']").fancybox();
    });
})(jQuery, window);    
</script>
{capture name=path}Obras{/capture}
<h1 class="text-center titulo">Salón Ritz Hotel Dann Carlton</h1>
<div class="row row-centered">
    <div class="col-md-6 col-centered">        
        <ul id="galeria">
            <li data-thumb="{$base_dir}/imagenes/obras/ritz/001.jpg" data-src="{$base_dir}/imagenes/obras/ritz/001.jpg"><a href="{$base_dir}/imagenes/obras/ritz/001.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/ritz/001.jpg" /></a></li>
        </ul>
    </div>
</div>
<br />
<br />
<br />
