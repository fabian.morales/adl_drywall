<script>
(function($, window){
    $(document).ready(function() {
        $('#galeria').lightSlider({
            gallery:true,
            item:1,
            loop:true,
            thumbItem:5,
            slideMargin:0,
            enableDrag: true,
            currentPagerPosition:'left',
            controls: true,
            onSliderLoad: function(el) {
                el.lightGallery({
                    selector: '#galeria .lslide',
                    onSlideClick: function() {
                        alert('lslsls');
                    }
                });
            }   
        });
        
        //$("a[rel='fancy']").fancybox();
    });
})(jQuery, window);    
</script>
{capture name=path}Obras{/capture}
<h1 class="text-center titulo">Centro Comercial Unico Cali</h1>
<div class="row row-centered">
    <div class="col-md-6 col-centered">        
        <ul id="galeria">
            <li data-thumb="{$base_dir}/imagenes/obras/unicocali/001.jpg" data-src="{$base_dir}/imagenes/obras/unicocali/001.jpg"><a href="{$base_dir}/imagenes/obras/unicocali/001.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicocali/001.jpg" /></a></li>
            <li data-thumb="{$base_dir}/imagenes/obras/unicocali/002.jpg" data-src="{$base_dir}/imagenes/obras/unicocali/002.jpg"><a href="{$base_dir}/imagenes/obras/unicocali/002.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicocali/002.jpg" /></a></li>
            <li data-thumb="{$base_dir}/imagenes/obras/unicocali/003.jpg" data-src="{$base_dir}/imagenes/obras/unicocali/003.jpg"><a href="{$base_dir}/imagenes/obras/unicocali/003.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unicocali/003.jpg" /></a></li>
        </ul>
    </div>
</div>
<br />
<p><strong>Obra: </strong> Ampliación Centro Comercial Unico Cali</p>
<p><strong>Uso Edificación: </strong>Comercial</p>
<p><strong>Ciudad: </strong>Cali</p>
<p><strong>Fecha de Inicio: </strong>Octubre de 2013 </p>
<p><strong>Fecha de Entrega: </strong>Febrero de 2014 </p>
<p><strong>Productos: </strong>Cielos drywall, muros divisorios drywall, fachadas superboard y fachadas hunter douglas.</p>

