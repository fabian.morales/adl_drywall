<script>

(function($, window){

    $(document).ready(function() {

        $('#galeria').lightSlider({

            gallery:true,

            item:1,

            loop:true,

            thumbItem:5,

            slideMargin:0,

            enableDrag: true,

            currentPagerPosition:'left',

            controls: true,

            onSliderLoad: function(el) {

                el.lightGallery({

                    selector: '#galeria .lslide'

                });

            }   

        });

        

        $("a[rel='fancy']").fancybox();

    });

})(jQuery, window);    

</script>

{capture name=path}Obras{/capture}

<h1 class="text-center titulo">Centro Comercial Unicentro Pasto</h1>

<div class="row row-centered">

    <div class="col-md-6 col-centered">        

        <ul id="galeria">

            <li data-thumb="{$base_dir}/imagenes/obras/unipasto/001.jpg" data-src="{$base_dir}/imagenes/obras/unipasto/001.jpg"><a href="{$base_dir}/imagenes/obras/unipasto/001.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unipasto/001.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/unipasto/002.jpg" data-src="{$base_dir}/imagenes/obras/unipasto/002.jpg"><a href="{$base_dir}/imagenes/obras/unipasto/002.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/unipasto/002.jpg" /></a></li>

        </ul>

    </div>

</div>

<br />

<p align="justify"><strong>Obra: </strong>Centro Comercial Unicentro Pasto</p>
<p align="justify"><strong>Uso Edificación: </strong>Comercial</p>
<p align="justify"><strong>Ciudad: </strong>Pasto</p>
<p align="justify"><strong>Fecha de Inicio: </strong>Abril 28 de 2009</p>
<p align="justify"><strong>Productos: </strong>Muros internos, muros para fachada y cielo falso en panel yeso y fibrocemento.</p>


