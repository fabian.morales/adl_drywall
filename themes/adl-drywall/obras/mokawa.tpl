<script>

(function($, window){

    $(document).ready(function() {

        $('#galeria').lightSlider({

            gallery:true,

            item:1,

            loop:true,

            thumbItem:5,

            slideMargin:0,

            enableDrag: true,

            currentPagerPosition:'left',

            controls: true,

            onSliderLoad: function(el) {

                el.lightGallery({

                    selector: '#galeria .lslide'

                });

            }   

        });

        

        $("a[rel='fancy']").fancybox();

    });

})(jQuery, window);    

</script>

{capture name=path}Obras{/capture}

<h1 class="text-center titulo">Mocawa Plaza</h1>

<div class="row row-centered">

    <div class="col-md-6 col-centered">

        <ul id="galeria">

            <li data-thumb="{$base_dir}/imagenes/obras/mokawa/001.jpg" data-src="{$base_dir}/imagenes/obras/mokawa/001.jpg"><a href="{$base_dir}/imagenes/obras/mokawa/001.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/mokawa/001.jpg" /></a></li>

            <li data-thumb="{$base_dir}/imagenes/obras/mokawa/004.jpg" data-src="{$base_dir}/imagenes/obras/mokawa/004.jpg"><a href="{$base_dir}/imagenes/obras/mokawa/004.jpg" rel="fancy"><img src="{$base_dir}/imagenes/obras/mokawa/004.jpg" /></a></li>

            
                </ul>

    </div>

</div>

<br />

<p align="justify"><strong>Obra: </strong> Mocawa Plaza - Armenia</p>
<p align="justify"><strong>Uso Edificación: </strong>Comercial, Residencial, Hotel.</p>
<p align="justify"><strong>Ciudad: </strong>Armenia</p>
<p align="justify"><strong>Fecha de Inicio: </strong>Enero de 2013 </p>
<p align="justify"><strong>Fecha de Entrega: </strong>Diciembre de 2013 </p>
<p align="justify"><strong>Productos: </strong>Cielos drywall, muros divisorios drywall y fachadas superboard.</p>


