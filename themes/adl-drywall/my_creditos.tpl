{capture name=path}Solicitudes de cr&eacute;dito{/capture}
<div class="container">
    <div class="row">
        <div class="col-sm-12 text-center">
            <h1 class="titulo">Solicitudes de cr&eacute;dito</h1>
        </div>        
        <div class="col-sm-6 col-md-4 col-lg-3"><a href="{$basedir}pdf/1.formato_solicitud_credito.pdf" class="btn btn-default btn-block pdf" target="_blank">Formato de solicitud de crédito</a></div>
        <div class="col-sm-6 col-md-4 col-lg-3"><a href="{$basedir}pdf/2.pagare_y_carta_instrucciones_credito_directo.pdf" class="btn btn-default btn-block pdf" target="_blank">Pagaré y cartas de instrucciones - Crédito directo</a></div>
        <div class="col-sm-6 col-md-4 col-lg-3"><a href="{$basedir}pdf/3.pagare_y_carta_instrucciones_credito_persona_natural.pdf" class="btn btn-default btn-block pdf" target="_blank">Pagaré y cartas de instrucciones - Crédito persona natural</a></div>
        <div class="col-sm-6 col-md-4 col-lg-3"><a href="{$basedir}pdf/covinoc_autorizacion_adl.pdf" class="btn btn-default btn-block pdf" target="_blank">Covinoc - Autorización ADL</a></div>
        <div class="col-sm-6 col-md-4 col-lg-3"><a href="{$basedir}pdf/covinoc_formato_solicitud_de_credito.pdf" class="btn btn-default btn-block pdf" target="_blank">Covinoc - Formato de solicitud de crédito</a></div>
    </div>
</div>

