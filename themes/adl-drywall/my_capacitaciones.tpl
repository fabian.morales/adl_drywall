{capture name=path}Capacitaciones{/capture}
<h1 class="text-center titulo">Capacitaciones</h1>
<p align="justify">Entendemos que una de las mejores maneras de apoyar su labor es mediante la capacitación y 
    actualización en los temas de construcción liviana. Por esto, en asocio con el SENA y proveedores, 
    realizamos cursos de capacitación en cada una de las ciudades donde tenemos presencia. Consulte 
    los temas de capacitación y sus fechas programadas, para que se inscriba en el punto de ventas 
    más cercano.</p>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3">
                <a href="{$base_dir}/index.php?controller=custom&vista=capacitaciones4">
                    <div class="obras"><img class="img-responsive" src="{$base_dir}/imagenes/capacitaciones4/Adl_Drywall_Capacitacion_Gyplac_01.jpg" /></div><span>Ahorra costos y tiempo en obra con el sistema liviano - Gyplac</span>
                </a>
            </div>
            <div class="col-md-3">
                <a href="{$base_dir}/index.php?controller=custom&vista=capacitaciones3">
                    <div class="obras"><img class="img-responsive" src="{$base_dir}/imagenes/capacitaciones3/01.jpg" /></div><span>Capacitaci&oacute;n Tejas Skinco - Sena</span>
                </a>
            </div>
            <div class="col-md-3">
                <a href="{$base_dir}/index.php?controller=custom&vista=capacitaciones2">
                    <div class="obras"><img class="img-responsive" src="{$base_dir}/imagenes/capacitaciones2/01.jpg" /></div>
                    <span>2do curso de Actualización e instalación</span>
                </a>
            </div>
            <div class="col-md-3">
                <a href="{$base_dir}/index.php?controller=custom&vista=capacitaciones1">
                    <div class="obras"><img class="img-responsive" src="{$base_dir}/imagenes/capacitaciones1/01.jpg" /></div>
                    <span>1er curso de Actualización e instalación</span>
                </a>
            </div>
        </div>
    </div>
</div>
<br />
<br />
<br />